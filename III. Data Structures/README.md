Sets are as fundamental to computer science as they are to mathematics.  
Whereas mathematical sets are unchanging, they sets manipulated by algorithm can grow, shrink, or otherwise change overtime. We call such sets **dynamic**.  
Two categories of operations:  
- **queries**    
- **modifying operations**  

**Overview of Part III:**
- Chapter 10: stacks, queues, linked lists, and rooted trees.
- Chapter 11: Hash tables.  
- Chapter 12: Binary Search Tree.  
- Chapter 13: red-black trees.  
- Chapter 14: advanced red-black trees.  

