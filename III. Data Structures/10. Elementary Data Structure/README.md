# 10. Elementary Data Structures.  
**Note:** Personally, these data structures is the basic of learning algorithm, and we(editors) certainly learnt them. So I won't put lots of works in this section.  

## 10.1. Stacks and queues.  
Stacks and queues are dynamic sets.  
**Stacks:**
The principle of stack is: **LIFO(last-in first-out)**.  
PUSH procedure:
```
PUSH(S, x):
    S[++S.size] = x
```
POP procedure:
```
POP(S):
    if (S.size == 0) Return "underflow"
    return s.size--
```
**Queues**  
The principle of queue is: **FIFO(First in first out)**.    

## 10.2. Linked lists.  
A ***linked list*** is a data structure in which the objects are arranged in a linear order. Unlike an array, in which the linear order is determined by the array indices, the order in a linked list is determined by a pointer in each object.  
We have many types of linked lists including: single linked list, double linked list, sorted linked list, circular linked list.  
**Linked list structure**
```
STRUCT Node:
    pointer next,prev
    int key
```

```
STRUCT LinkedList
    node head
```
**Searching in linked list**  
```
LIST-SEARCH(L,k):
    x = L.head
    while (x != NULL abd x.key != k)
        x = x.next
    return x.
```    
**Inserting in linked list**  
Given an element x, then insert it into the front of the list.  
```
LIST-INSERT(L,x):
    x.next = L.head
    if (L.head != NULL)
        L.head.prev = x
    L.head = x
    x.prev = NULL
```  
**Deleting from a linked list**  
Remove an "node" x from the linked list.
```
LIST_DELETE(L,x):
    if x.prev != NULL
        x.prev.next = x.next
    else L.head = x.next
    if x.next != NULL
        x.next.prev = x.prev
```  
## 10.3. Implementing pointers and objects.  (Read more in pages 262 - 264)  
## 10.4. Represent rooted trees.  
In this section, we will look at how to represent a tree use linked list.  
**Binary trees:**  
NODE struct:  
```
STRUCT Node:
    pointer lchild, rchild, top
    int key
```
B-Tree struct:
```
STRUCT B-Tree
    Node root
```  
![B-tree](B_tree.png)  
**Rooted trees with unbounded branching**  
We can extend the scheme for representing a binary tree to any class of tree in which number of children of each node are unlimited.  
Instead of create numerous child_pointers like: child1, child2, ... child_k, we can save our spaces and time by saving only left-child and right-sibling:  
- x.left-child points to the leftmost child of the node.  
- x.right-sibling point to the sibling of x immediately to its right.
```
STRUCT Node:
    pointer leftchild, right-sibling;
    pointer top
    int key
```  
![tree](tree.png)  
**Other tree representations**  
We sometimes represent rooted trees in other ways. In chapter 6, for example, we represented a heap, which is based on a complete binary tree, by a single plus the index of the last node in the heap. The trees that appear in Chapter 21 are traversed only toward the root, and so only the parent pointers are present, there are no pointer to children.  
Many other schemes are possiblem and which scheme is best depends on the application.  

 




