# 12. Binary Search Trees.  
## 12.1. What is a binary seach tree.  
A **binary search tree** is organized, as the name suggests, in a binary tree, as shown in Figure 12.1.
We can represent such a tree by a linked data structure, in which each note is an object.  
```
struct Node
    int key
    node left, right, p
    // p = parent
```
If a node x doesn't have the parent (which means x.p == NULL), it means that it is a root.  

![binary search tree](binary-search-tree.png)  
Binary search tree holds a simple principle:
> Let x be a node in a binary search tree. If y is a node in the left subtree of x, then y.key $\leq$ x.key. If y is a node in the right subtree of x, then y.key $\geq$ x.key.  

The binary-search-tree property allows us to print out all the keys in a binary search tree in sorted order by a simple recursive algorithm, called an **inorder tree walk**.  
```
INORDER-TREE-WALK(x)
    if x != NULL
    INORDER-TREE-WALK(x.lef)
    print x.key
    INORDER-TREE-WALK(x.right)
```  
When we call **INORDER-TREE-WALK(x)**, we need to remember that all nodes in the left subtrees is lower than x, which means, we need to print them out first, then x.key, and finally is all the key values in the right subtrees.  
**Theorem 12.1**
> If x is the root of an n-node subtree, then the call INORDER-TREE-WALK(x), takes $\Theta(n)$ time.  

**Proof:** We can see that we calls recursive at most n times, and each time we only take $\Theta(1)$ time. Therefore, our answer is $\Theta(n)$.  
**Exercises:**  
**12.1-2:** What is the different between the binary search-tree property and the min-heap property. Can the min-heap property be used to print out the keys of an n-node tree in sorted order in O(n) time?. Show how, or why not?.  
**Solution:**
- Binary search tree holds:
> Let x be a node in a binary search tree. If y is a node in the left subtree of x, then y.key $\leq$ x.key. If y is a node in the right subtree of x, then y.key $\geq$ x.key.  

- min-heap property states that:
> A[parent(i)] $\leq$ A[i] while i is a node in the min-heap tree  

In order to sort the min-heap tree, we need $O(nlog(n))$ time.  
## 12.2. Querrying in a binary search tre.  
In this section, we will examine these operation: SEARCH, MINIMUM, MAXIMUM, SUCCESSOR, PREDECESSOR. All of these takes: O(h) which h is the height of the tree.  
**Searching**  
We can search a value k in the binary search tree, with an approach which is quite similar with **binary seach**.  
```
TREE-SEARCH(x, k):
    if x == NULL OR K == X.key return x.
    if k < x.key //(which means k belongs to the left sub tree) 
        return TREE-SEARCH(x.left, k)
    return TREE-SEARCH(x.right,k)
```  
Or we can rewrite in an iterative fashion by "unrolling" the recursion into a while loop.  
```
ITERARTIVE-TREE-SEARCH(x,k)
    while x != NULL and k != x.key
        if k <= x.key 
            x = x.left
        else: 
            x = x.right
```
**Minimum and maximum.**  
The minimum value is the left most node, while the maximum value is the right most node.  
MINIMUM procedure:  
```
TREE-MINIMUM(x)
    while(x.left != NULL)
        x = x.left
    return x
```  
MAXIMUM procedure:
```
TREE-MAXIMUM(x):
    while (x.right != NULL)
        x = x.right
    return x.
```  
**Successor and predecessor**  
Given a node in a binary search tree, sometimes, we need to find its successor in the sorted order determined by an inorder tree walk.  
If all keys are distinct, the successor of a node x is the node with the smallest key greater than x.key.  
```
TREE-SUCCESSOR(x):  
    if x.right != NULL
        return TREE-MINIMUM(x.right)  
    y = x.p
    while (y != NULL and x == y.right)
        x = y
        y = y.p
```
We break the code into 2 cases:
- If the right subtree of x is nonempty,  then the successor of x is just the leftmost node in x's right tree. We can find it by using: `TREE-MINIMUM(x.right)`.  
- Otherwise, then we need to find successor y of x, where is y is the lowest ancestor of x whole left child is also an ancestor of x.  

![successor](successor.png)  

In **Figure 12.2**, we can see that the node with key 13 has no right subtree, and thus its successor is its lowest ancestor whose left child is also an ancestor. In this case, the node with key is its successor.  
## 12.3. Insertion and deletion.  

### Insertion  
To insert a new value v into a binary search tree T, we use the procedure `TREE-INSERT`. The procedure takes a node z for which z.key = v, z.left = z.right = NULL.  
It modifies T and some of the attributes of z in such way that it inserts z into an appropriate position in the tree.  
```
TREE-INSERT(T,z):
    y = NULL
    x = T.root
    while x != NULL
        y = x.
        if z.key < x.key
            x = x.left 
        else 
            x = x.right
    z.p = y
    if y == NULL 
        T.root = z
    else if z.key < y.key
        y.left = z
    else y.right = z.
```  
![insertion](insertion.png)  
In other way, we devide the insertion progress into two main steps.  
+ Find a leaf who should be the parent of the node z by using `TREE-SEARCH`-like approach.  
```
    y = NULL
    x = T.root
    while x != NULL
        y = x.
        if z.key < x.key
            x = x.left 
        else 
            x = x.right
    z.p = y
```
+ Call the parent is y. then we need to insert node z as the new child of parent y.  
```
    if y == NULL 
        T.root = z
    else if z.key < y.key
        y.left = z
    else y.right = z.
```  
### Deletion.  
The overall strategy for deleting a node z from a binary search tree T has 3 basis cases but, as we shall see, one of the cases is a bit tricky.
+ If z has no children, then we simply remove it by modifying its parent to replace z with NULL as its child.
+ If z has just one child, then we elevate that child to take z's position in the tree by modifying z's parent to replace z by z's child.  
+ If z has two children, then we find z's successor y - which must be in z's right subtree and have y take z's position in the tree and remain the order of the tree. This case is the tricky one because, as we shall see, it matters whether y is z's right child.  

`TRANSPLANT(T,u,v)` procedure replaces one subtree as a child of its parent with another subtree. It means that we delete node u and made v as a replacement for u.  
```
TRANSPLANT(T,u,v):
    if u.p == NULL
        T.root = v.
        // if u == ROOT.
    else if u == u.p.left // if u is left child of its parent 
        u.p.left = v
    else u.p.right = v
    if v != NULL
        v.p = u.p
```  

With the TRANSPLANT procedure in hand, here is the procedure that deletes node z from binary search tree T.  
```
TREE-DELETE(T,z):
    if z.left == NULL // if z have only a right child.
        TRANSPLANT(T,z,z.right) 
    else z.right = NULL
        TRANSPLANT(T,z,z.left)
    else 
        y = TREE-MINIMUM(z.right) // find z's successor.
        if y.p != z // y != z.right.
            TRANSPLANT(T,y,y.right)
            y.right = z.right
            y.right.p = y
        TRANSPLANT(T,z,y)
        y.left = z.left
        y.left.p = y
```  
For more detail, please look at the (d) in the **Figure 12.4** below.  

![deletion](deletion.png)  
## 12.4. Randomly built binary search trees. (*).  
We have shown that each of the basic operations on a binary search tree runs in O(h) time, where h is the height of the tree.  
The height of a binary search tree varies from [lg n] to n-1. As with quicksort, we can show that the behavior of the average case is much closer to the best case to the worst case.  
In this section, we will excute the average height of binary search tree in the situation when the tree is built by **insertion** alone.  
Let us therefore define a **randomly built binary search tree** on n keys as one that arises from inserting the keys in random order into an initially empty tree, where each of the n! permutations of the input keys is equally likely.  
In this section, we shall prove the following theorem.  
**Theorem 12.4**  
> the expected height of a randomly built binary search tree on n distinct keys is O(lg n).  
**Proof**  
We start by defining 3 random variables that help measure the height of a randomly built binary search tree.
+ We denotes the height of a randomly built binary search tree on n keys by $X_{n}$.  
+ Define the **exponetial height** $Y_{n} = 2^{X_{n}}$
+ When we build a binary search tree on n keys, we choose one key as that of the root, and we let $R_{n}$ denote the random variable that holds this key's rank within the set of n keys; that is, $R_{n}$ holds the position that this key would occupy if the set of keys were sorted. The value of $R_{n}$ is equally likely to be any element of set {1,2...n}.  
If $R_{n}$ = i, then the left subtree of the root is a root is a randomly built binary search tree on i - 1 keys, and the right subtree is a randomly built binary search tree on n - i keys. Because the height of a binary tree is 1 more than the larger of the heights of the two subtrees of the root, the exponential height of a binary tree is twice.  

$$Y_{n} = 2 . max(Y_{i-1}, Y_{i})$$

As a base cases, we have that $Y_1 = 1$, $Y_0 = 0$  
Next, define indicator random variables $Z_{n,1}, Z_{n,2}, ... Z{n,n}$ where:

$$Z_{n,i} = I(R_{n} == i)$$

Because $R_{n}$ is equally likely to be any element of {1,2...,n} it follows that Pr{$R_{n}=i$} = $\frac{1}{n}$ for i = 1,2,...n, and hence, we have:  

$$ E[Z_{n,i}] =  1/n. (12.1) $$

Because exactly one value of $Z_{n,i}$ is 1 and all others are 9, we also have:  

$$Y_{n} = \sum_{i=1}^{n} Z_{n,i}(2. max(Y_{i-1}, Y_{n-i}))$$  
We shall show that E[$Y_{n}$] is polynomial in n, which ultimately imply that E[$X_{n}$] = O(lg n).  
Then we have these formulars:  
![prove1](prove1.png)  
Using the substitution method, we shall show that for all positive integer n, the recurrence (12.2) has the solution.

$$ E[Y_{n}] \leq \frac{1}{4} {n+3 \choose 3}$$  

In doing so, we shall use the identity(call as equation 12.3):     

$$\sum_{i=0}^{n-1} {(i+3) \choose 3} = {(n+3) \choose 4}$$
For the base cases, we note that the bounds 0 = $Y_{0} = E[Y_0] \leq (1/4){3 \choose 3} = 1/4$ and $1 = Y_1 = E[Y_1] \leq (1/4) {(1+3) \choose 3} = 1$  

![prove2](prove2.png)  

Then by employing Jensen's inequality (C.26), we have:

$$2^{E[X_n]} <= E[2^{X_n}] = E[Y_n]$$

as follows:

$$2^{E[X_n]} \leq \frac{1}{4} {(n+3) \choose 3} = \frac{n^3 + 6n^2 + 11n + 6}{24}$$  

Taking logarithms of both sides gives $E[X_n] = O(lg n)$.  
**Exercise**  
**12.4-1** Prove equation (12.3).  
**Solution:**  
Let consider all the possible positions of the largest element of the subset n+3 of size 4. Suppose it is i+4, then we have i+3 position left to choose 3 smaller elements. By adding them up, we have the final equation.  




















