# 11. Hash tables
*Read chapter 10 - "linked list" before reading this*  

*To keep the spirit of a summary text, from now on I will truncate all the proof. However, the beauty of the book is in those proof so I really recommend you to read it.*  

## 11-1. Direct-addressing table (page 254)
*Concept*  

Direct-addressing table works well when the universe *U* of keys is reasonalbly small.  
Suppose application has a dynamic set in which each element has a key drawn from universe $U=\{1, 2, ..., m-1\}$ ($m$ is not too large and no 2 elements have same key).  
To represnet the dynamic set, we use an array called **direct-addressing table** in which each position, or ***slot***, correspond to a key in the universe *U*: 
* Slot $k$ points to the element in the set with key $k$
* If the slot has no element with key $k$, then $T[k]=NIL$

*Implementation*  

``` 
DIRECT-ADDRESS-SEARCH(T,k)
    return T[k]
```
```
DIRECT-ADDRESS-INSERT(T, x)
    T[x.key] = x
```
```
DIRECT-ADDRESS-DELETE(T,x)
    T[x.key] = NIL
```
![Direct-addressing illustration](direct-addressing-table.PNG)

*Upgrade*

Some direct-addressing table stores in the slot the pointer to an external object containing the *key* and the *satallite data*.    
Because the key is the index, some directly stores the object in the table. Then there must be a special key to tell a slot is empty.  
Some stores only the satellite data in the slot. However, then. there must be another way to tell whether the slot is empty.

## 11.2 Hash tables
*Introduction*  

Direct addressing downside is if $U$ is too large, then the memory might not be enough. Furthermore, reality, set $K$ of keys actually stored is relatively small compared to $U$ so memory allocated for the table might be wasted.  
In that case, we use hash table: 
* Storage requirement: $\Theta(|K|)$
* Searching element time (*averagely* while direct address is *worst-case*): $O(1)$  

*Concept*  

With direct-addressing, an element key $k$ is stored in slot $k$.  
With hashing, this element is stored in slot $h(k)$; we use a **hash function** to compute the slot $h(k)$ for key $k$. Here, *h* maps the universe $U$ of keys into slots of *hash table* $T[0..m-1]:  

$$h: U \rightarrow \{0, 1,..., m-1\}$$

A hash function must be **deterministic**: one key $k$ corresponds to one hash value $h(k)$.  
Size $m$ of table is much less than $|U|$.  
We say that an element with key $k$ **hashes** to slot $h(k)$, and $h(k)$ is the **hash value** of key $k$.

![Hash table illustration](hash-table.PNG)

*Problems*  

**Collision** is possible: 2 key hashes to the same slot. Because $|U| > m$, avoiding collision is impossible. We might minimize it by making $h$ appear to be "random", or deal with it using **chaining**.

### **Collision resolution by chaining**

In **chaining**, we put all the elements hashing to the same slot in the same linked list. Slot $j$ contains a pointer to the head of the linked list of all elements hashes to splot $j$; if there are no such elements, slot $j$ contains NIL.

![Chaining](chaining.PNG)

*Implementation*  
```
CHAINED-HASH-INSERT(T, x)
    insert x at the head of the list T[h(x.key)]
```
```
CHAINED-HASH-SEARCH(T,k)
    search for an element with key k in list T[h(x.key)]
```
```
CHAINED-HASH-DELETE(T, x)
    delete x from list T[h(x.key)]
```

**Insertion** worst-case running time is $O(1)$, assuming element $x$ being inserted is not already in the list. If we want to check this assumption, we have to add additional cost for searching element with key $x.key$.  
**Searching** worst-case running time proportional to the length of the list. Details in beelow.  
**Deletion** takes $O(1)$ if the lists are doubly linked list. Note that the `delete` procedure take the address $x$ not the key $x.key$, which means we do not need to search for $x$ in the delete function. In addition, using a singly linked list will results in $O(n)$ for delete since we have to go from begining to the previous element to delete.  

*Analysis hashing with chaining*  

Given a hash tables $T$ with $m$ slots that stores $n$ elements., we define **load factor** $\alpha = n/m$ for $T$, that is the average number of elements stored in one chain.  

The *worse-case* behaviour of chaining is all $n$ keys hashes to the same slot, creating a list of length $n$ - just like we use a linked list for all elements. *Searching* takes $\Theta(n)$ time plus time to calculate hash function.  

The *average-case* performace depends on how well the hash function $h$ distributes the set of keys to be stored among $m$ slots, on the average. For now, we assume any elements are **equally likely** to hash to any of the $m$ slot, *independently* where other elements hash to. We call this the assumption of ***simple uniform hashing***.  Section 11.3 discuss more about this.  
For $j = 0, 1, ..., m-1$, we denote the length of the list $T[j]$ by $n_j$, so $n = n_0 + n_1 + ... + n_{m-1}$ and the expected value of $n_j$ is $E[n_j] = \alpha = n/m$.  
We assume that $O(1)$ time suffices to compute the hash values $h(k)$. We shall examine the number of elements checked by the *searching* algorithm. There are 2 cases: unsuccessful search: no element in table $T$ has key $k$, and successfull search: finds element with key $k$.  

***Theorem 11.1***  
In hash-table in which collisions are resolved by chaing, under the assumption of simple uniform hasing, an *unsuccessful* search takes average-case time $\Theta(1+\alpha)$.  

*Proof on page 259. I recommend to read it.*

Successful search are slightly different. The list are *not* equally likely to be searched, so the probability of the list being searched is proportional to the number of elemetns it contains.

***Theorem 11.2***  
In hash-table in which collisions are resolved by chaing, under the assumption of simple uniform hasing, an *successful* search takes average-case time $\Theta(1+\alpha)$.  

*Proof on page 260*

These ananlysis means, if the number of hash-table slots is proportional to the number of elements: $n = O(m)$, consequently $\alpha=n/m=O(m)/m=O(1)$. Thus, searching takes *constant* time. With *insertion* takes worst-case $O(1)$, *deletion* takes worst-case $O(1)$ when the list are doubly linked list, we can support all dictionary operations in $O(1)$ on average.  

## 11.3 Hash function
*A good hash function?*  

A good hash function satisfies (approximately) the assumption of simple uniform hashing: each key is equally likely to hash into any of the $m$ slots, independent of what other keys hash into. Unfortunately, we typically have no way to check this condition because we do not know the probability distribution from which the keys are drawn. Also, the key might not be drawn independently.  
Occasionally we now the distribution. For example, if we know that keys are random real number $k$ independently and uniformedly distributed in range $0 \leq k < 1$, then the following hash function satisfies simple uniform hashing:

$$h(k) = \lfloor km \rfloor$$

In practice, we often employ our own technique to create a hash function that performs well. Qualititive information about keys maybe useful in the design process. For example, consider compiler's symbol table. Closely related symbols `pt` and `pts` must hash to different slot.  
A good approach derives hash value in a way independent of any patterns that might exist in the data, such as "division method".  
Some applications of hash function might require stronger properties than are provided with simple uniform hashing, such as universal hashing.

### *Intepreting keys as natural numbers*
Most hash function assumes that the universe of keys is the set $\mathbb{N} = \{0, 1, 2, ...\}$ of natural numbers.  
So, if keys are not natural numbers, we find a way to interprete them as natural numbers. For example, with an ASCII-character string, we turn them into radix-128 integer. $pt_{128} = (112 \cdot 128 + 116)_{10} = 14452_{10}$  
So, in what follows we assume keys are natural numbers.  

#### 11.3-1 The division method
*Implementation*  

The key $k$ hashes to the value of remainder of $k$ divided by $m$. The hash function is:

$$h(k) = k \text{ mod } m$$

For example, $m = 12$, $k = 100$ then $h(k) = 4$. It requires only 1 division, so it is quite fast.  

*Choosing m*

When using this method, we avoid some certain values of $m$:
* $m$ should not be $2^p$: since in this case, the hash value is just the lowest $p$ bit of $k$. If we do not know that whether the lowest $p$ bits are equally liekly, we sohuld hash to other values dependent on all bits.  
* $m$ should not be $2^p - 1$ when $k$ is character string: as exercise 11.3-3, a permutation of key $k$ results in the same hash value.
* $m$ should be *a prime not to close to the power of 2*.  

#### 11.3-2 The multiplication method
*Implementation*  

Involes 2 steps:  
1) Multiply key $k$ by a constant A in range $0 < A < 1$ and extract the fractional part.
2) Multiply this value by $m$ and take the floor of the result.  

In short:

$$h(k) = \lfloor m(kA \text{ mod } 1) \rfloor$$

where "$kA \text{ mod } 1$" means fractional part of A: $kA - \lfloor kA \rfloor$.

*Choosing m*

One advantage of this method is that the value of $m$ is not too critical. We typically choose $m = 2^p$ for some integer $p$, since we can implement easily in any computer as follows: suppose the word size of the machine is $w$ and $k$ fits into a single words. We restrict $A$ to be a fraction of form $s / 2^w$, with $0 < s < 2^w$. Multiply $k$ with $s$, we get a 2-word values in form of $r_1 \cdot 2^w + r_0$. With $m=2^p$, take the $p$ most significant bits of $r_0$, we get the hash value.

![multiplication method illustration](multiplication-method.PNG)

**Explanation**:  

$kA = (k \cdot s) / 2^w = (r_1 \cdot 2^w + r_0) / 2^w = r_1 + r_0 / 2^w$  
$\lfloor kA \rfloor = \lfloor r_1 + r_0 / 2^w \rfloor = r_1$ ($r_0 < 2^w$)  
Therefore:  
$kA - \lfloor kA \rfloor = r_0 / 2^w$  
Finally:  
$h(k) = m \cdot r_0 / 2^w = 2^p \cdot r_0 / 2^w = r_0 / 2^{w-p}$  
Which means we cut $w - p$ least significant bits, results us in $p$ most significant bits as the hash value.  

Although this method works with any value of $A$, the optimal choice depends on the characteristics of the data being hashed. Knuth suggest a value of $A$ works resonably well:

$$A \approx (\sqrt{5} - 1)/2$$

#### 11.3-3 Universal hashing  
*Problems*  

With a fixed hash function, input can result in $n$ keys hash to the same slot, therefore leads to a worst-case $\Theta(n)$ running time. Any fixed hash functions are vulerable to this, the *only* effective improving way is to choose *randomly* the hash function in a way that is *independent* of the keys and are actually stored. This has good performance on average, no matter the input.  

*Concept*  

In ***universal hashing***, at the begin of the execution, we *randomly* select a hash function from a carefully designed class of functions.  
Let $\mathscr{H}$ be a finite collection of hash functions that map a given universe $U$ of keys into range ${0, 1, 2, ..., m-1}$. Such a collection is said to be **universal** if for a pair of key $k, l \in U$, the number of hash functions $h \in \mathscr{H}$ for which $h(k) = h(l)$ is at most $|\mathscr{H}|/m$. In the other words, with a hash function chosen randomly from $\mathscr{H}$, a chance that the hash value of $k$ and $l$ collides is at most $1/m$.

***Theorem 11.3***

Suppose a hash function $h$ is chosen randomly from a universal collection of hash functions and has been used to hash a set of $n$ keys to a hash table $T$ of $m$ slots, resolving collision by chaining. If $k$ is not in the table, then the expected length of $E[n_{h(k)}]$ of the list that $k$ hashes to is at most the load factor $\alpha = n/m$. If $k$ is in the table, the expected length of $E[n_{h(k)}]$ of the list containing key $k$ is at most $1 + \alpha$.

*Proof on page 265*

Corrolary 11.4  

Using universal hashing and collision resolution by chaining in an initially empty table of $m$ slots, it takes an expected time of $\Theta(n)$ to handle any sequence of $n$ INSERT, SEARCH, and DELETE operations containing $O(m)$ INSERT operations.

*Proof on page 266*

#### **Design a universal class of hash functions**  

We begin by choosing a prime $p$ large enough so that every possible $k$ is in range $0$ to $p-1$. Let $\mathbb{Z}_p = {0,1,...,p-1}$ and let $\mathbb{Z}^*_p={1,2,...,p-1}$. Becasue we assume the universe keys is larger than the number of slots $m$ of the table, $p > m$.  
We now define a hash function $h_{ab}$ for any $a \in \mathbb{Z}^*_p$ and $b \in \mathbb{Z}_p$:  

$$h_{ab}(k) = ((ak + b) \text{ mod } p) \text{ mod }m$$

The family of such hash functions is:

$$\mathscr{H}_{pm} = {h_{ab}: a \in \mathbb{Z}^*_p \text{ and } b \in \mathbb{Z}_p}$$  

The collection $\mathscr{H}_{pm}$ contains $p(p-1)$ hash functions. Each hash function $h_{ab}$ maps $\mathbb{Z}_p$ to $\mathbb{Z}_m$. A nice property is that $m$ is arbitrary - not necessarily be prime.

***Theorem 11.5***  
The class functions $\mathscr{H}_{pm}$ is universal.  

*Proof on page 267*

## 11.4 Open addressing
*Concept*  

On open addressing, all elements occupy the table itself - each table entry either contains an element or `NIL`. When searching for an element, we systematically examine the table slots until either find the desired element or ascertain that the element is not in the table. *No lists and no elements are stored outside the table.* Thus, hash table fills up until no further insertion are possible; as a result, load factor $\alpha$ never exceeeds 1.  
The advantage of open addressing is that it avoids pointer: instead of following the pointer, we *compute* the sequence of slots to be examined. The extra memory by not storing pointer can substantially store more elements, potentially avoid collision and faster retrieval.  

*Concept*

To perform open-addressing insertion, we successively examine, or **probe** until we find an empty slot to put the element. However, instead of fixed order $0, 1,...,m-1$ (requiring $\Theta(n)$), the sequence of positions to be probed depends on the key being inserted. We extend the hash function adding the probe number (starting from 0) as second input :

$$h: U \times \{0,1,...,m-1\} \rightarrow \{0,1,...,m-1\}$$

With open addressing, we require every key $k$ has a **probe sequence** a permutation of $\{0,1,...,m-1\}$:

$$\{h(k,0), h(k,1), ..., h(k,m-1)\}$$

Thus, we will travel until we find an empty slot.

*Implementation*  

Assume all slots in the table $T$ are keys without no satellite data. Each slot contains either a *key* or `NIL`. The following HASH-INSERT procedure returns either an empty slot position or flag error.

```
HASH-INSERT(T,k):
i = 0
repeat
    j = h(k,i)
    if T[j] == NIL
        T[j] = k
        return j
    i = i + 1
until i == m
error "hash table over flow"
```

1 key $k$ has one probe sequence. So the same probe sequence is used to searching. As a result, the search can terminate (unsuccessfully) *when it finds an empty slot*. The procedure HASH-SEARCH takes a hash table $T$ and a key $k$ as an input, returning $j$ if $T[j]$ contains key $k$, and return `NIL` if $k$ is not present in the table.
```
HASH-SEARCH(T,k)
i = 0
repeat
    j = h(k,i)
    if T[j] == k:
        return j
    i = i + 1
until T[j] == NIL or i == m
return NIL
```
Deletion from open-addressing hash table is difficult. We cannot set the deleted slot to `NIL` since search stop at `NIL` empty slot. We can use another special flag `DELETED` instead of `NIL` for the deleted slot. That way, we need to update the INSERTION procedure to treat `DELETED` slot as an empty slot. The SEARCH needs no change, since it only cares about `NIL` and not `NIL`. However, in that case, search time no longer depends on the load factor $\alpha$, for this reason chaining is more commly a collision resolution technique if deletion operation is needed.  

*Analysis*  

We shall assume ***uniform hashing***: probe sequence of each key is generally equally likely to be any of the $m!$ permutation of $(0, 1, ..., m-1)$. *Uniform hasing* generalizes the idea of *simple uniform hasing* since it does not only create a number but a whole probe sequence. In practice, true uniform hasing is difficult to achieve, but approximation is achievable through these techniques. These techniques can only produce a maximum of $m^2$ probe sequence - not the $m!$ as desire - and among them **double hashing** gives out the most.  

#### Linear probing

Given an **auxiliary hash function** $h': U \rightarrow \{0,1,...,m-1\}$. The method of **linear probing**:  

$$h(k,i) = (h'(k) + i) \text{ mod }m$$  

for $i = 0, 1,..., m-1$. Given key $k$, we first probe $T[h'(k)]$, then $T[h'(k)+1]$, then $T[h'(k)+2]$, so on until we find empty slot or $T[h'(k)-1]$ which is the last probe. Because the initial probe decides the whole probe sequence, there are only $m$ distinct probe sequences.

$Problems$

Is to implement it is, linear probing suffers from a problem known as ***primary clustering***. Long runs of occupied slots build up, increae average search time. Clusters arise because an empty slot preceded by $i$ full slot has a probability of $(i+1)/m$ to be filled - which the longer we run the higher the chance, the bigger the clusters become, the even higher the chance.

#### Quadratic probing  

Quadratic probing uses a hash function of the form:

$$h(k,i) = (h'(k) + c_1i + c_2i^2)\text{ mod }m$$

where $h'$ is auxiliary function, $c_1$ and $c_2$ are positive auxiliary constants.  
The initial probed position is $T[h'(k)]$, later positions are offset by amounts depend in a quadritic manner on the probe number $i$. This method works better than linear probing, but the values $c_1$, $c_2$, $m$ are more constrained. (See problem 11.4-3 for one way to select those). Like linear probing, there are only $m$ distinct probe sequence.

$Problem$  

If two keys have the same inititial position: $h(k_1,0) = h(k_2,0)$ then their whole probe sequence are the same. This is called **secondary clustering** - a less severe problem.  

#### Double hashing  

***Double hasing*** uses function of the form:  

$$h(k,i) = (h_1(k) + ih_2(k))\text{ mod }m$$

where both $h_1$ and $h_2$ are auxiliary hash functions.  
Initial probe is $T[h_1(k)]$, each later position offset by its previous one an amout of $h_2(k)$, *modulo* $m$. Thus, unlike linear probing, the probe sequence depends on the inital probe and the offset.  
The value $h_2(k)$ must be relatively *prime* to the table size $m$ for the whole table to be searched - in another word, for the probe sequence to be a permutation of $0, 1, ..., m-1$. (see problem 11.4-4)  
One way is to design $m$ is a power of 2 while $h_2(k)$ returns an odd number.  
Another way is to design $m$ a prime number while $h_2$ always return an integer less than $m$. For example:  
$h_1(k) = k \text{ mod } m$  
$h_2(k) = 1 + (k \text{ mod } m')$$  
where $m'$ is an integer less than $m$, for example $m-1$.

*Advantage*  

When the condition is satisfied, **double hashing** improves over 2 above method that it uses $\Theta(n)$ probe sequences, rather than just $O(n)$, since each pair $(h_1(k),h_2(k))$ yields a different probe sequence.  
In practice, when $m = 2^p$, to produce $h_2(k)$ relatively prime to $m$ is difficult.  

#### Analysis of open addressing  

With open addressing, at most 1 element occupies 1 slot, so the load factor $\alpha = n/m \leq 1$. Assuming we are using uniform hasing, consider the distribution on the space of keys, eaach possible probe sequence is equally likely. Also a given key has a fixed probe sequence associated with it.  

***Theorem 11.6***  
Given an open-addressing hash tables with load factor $\alpha=n/m < 1$, the expected number of probe in an unsucessful search is at most $1/(1-\alpha)$, asssuming uniform hashing.  

*Proof page 274*  

***Corolarry 11.7***
Inserting an element into an open-address hash table with a load factor $\alpha$ requires at most $1/(1-\alpha)$ probes on average, assuming uniform hashing.  

***Theorem 11.8***  
Given an open-address table with a load factor $\alpha < 1$, the expected number of probe in a successful search is:  

$$\frac{1}{a}ln{\frac{1}{1-\alpha}}$$ 

assuming uniform hashing, and each key in the table is equally likely to be searched.  

*Proof on page 276*  

## 11.5 Perfect hashing 

Although have excellent *average-case* performance, hashing can result in worst-case performance when the set of keys is **static**: once the keys are stored in the table, the set of keys never change - which means every keys hash to the same slot. Some application just have static set of keys: like compiler's set of reserved word.  

*Perfect Hashing*  

We call a hashing technique ***perfect hashing*** if $O(1)$ *memory accesses* are required to perform a search in worst-case.  

*Concept*  

To conduct the perfect hashing scheme, we uses two levels of hashing, with *universal hashing* at each level.  
1. The first level is the same as hashing with chaining: we hash $n$ key $k$ to its corresponding slot using a hash function $h$ carefully chosen from a family of unniversal hash functions.  
2. Instead of a linked list, each slot is a ***secondary hash table*** $S_j$ asscociated with the function $h_j$. By choosing $h_j$ carefully, we guarantee no collision on secondary level.  

To guarantee no collison at secondary level, we need the size of $m_j$ of the hash table $S_j$ to be *square* of the $n_j$ number hashing to that slot. At first it sounds a little bit overwhelming on the memory space, we can actually limit the expected used space to $O(n)$.  
We use hash function choosen from the universal classes of hash functions:
1. The first function is choosen from the class $\mathscr{H}_{p,m}$, where $p$ is a prime number larger than any key.  
2. Those key hashes to slot $j$ are re-hashed into secondary table $S_j$ of size $m_j$ using hash function $h_j$ choosen from the class $\mathscr{H}_{p,m_j}$.  

***Theorem 11.9***  
Suppose we have $n$ keys stored in a hash table of size $m = n^2$ and a hash function $h$ choosen from a universal classes of hash functions. Then the probability of there are any collision is less than $1/2$.  

*Proof on page 279*

Which means, more than 50% that there are no collision. As a result, we just need to pick a random hash function $h$ fomr universal classes of hash functions until find a function with no collison. Averagely it takes 2 tries only. Amazing! But the $O(n^2)$ seems quite excessive. That is why we have 2 level of the scheme.

***Theorem 11.10***  
Suppose that we store $n$ keys into a hash table of size $m = n$ using a hash function $h$ choosen carefully from an universal classes of hash functions. Then, we have:  

$$E\Big[\sum_{j=0}^{m-1}n_j^2\Big] < 2n$$ 

where $n_j$ is the number of keys hash to slot $j$.  

*Proof on page 280*  

***Corrolary 11.11***  
The perfect hashing method propsed above required an expected memory space less than $2n$.

*Proof on page 281*

***Corrolary 11.12***  
with the perfect hashing method proposed above, the probability that the expected memory space exceeds or equal $4n$ is less than $1/2$.  

*Proof on page 282*

Which means, we just need to test a few random hash function $h$ until we found out a hash function that results in less than $2n$ memory space. Again, the average number of tries is $2$.
