# 13. Red-Black trees

## 13.1 A properties of red-black trees
A red-black trees is a binary search tree with one more info stored in every node: the color of the node - *red* or *black*. With the color constraining, the tree ensures that every path is at last twice longer than others, so approximately ***balanced***.  
Every node contains these attributes:  
- color
- key
- left
- right
- p

If a child or parent of a node does not exist, its corresponding pointer point to **NIL**. In the tree, we will regard these **NIL** nodes as the leaves an the external node of the trees and the key-bearing node is the internal node of the trees.  

A red-black tree is a binary trees satisfies these properties:
1. Every node is either red or black.
2. The root is black.
3. Every leaf (NIL) is black.
4. Every red node has both of its children black.
5. Every simple paths from a simple node its leaves has the same number of black nodes.  

We use one *sentinel* (page 238) T.nil to represent all the NIL in the tree.

***Black height***: define the number of black nodes in the simple paths from a node x to its child the ***black height*** of the node x: `bh(x)`. The black height of the tree is the black-height of the root.

***Lemma 13.1***  
A red-black tree with *n* internal nodes has height at most $2lg(n+1)$.

Sub-proof: any node x contains at least $2^{bh(x)} - 1$ internal nodes in its tree. (Proof using induction)  
So we have $n \geq 2^{bh(x)} - 1$
Let $h$ be the heights of the tree. Since properties 4, a simple path from root to leaves contains at most half of them red nodes, which means: $bh(x) \geq h/2$ so $n \geq 2^{bh(x)} - 1 \geq 2^{h/2} - 1$, or $2lg(n+1) \geq h$.

(*Proof at page 309*)  

From lemma, we know we can implement these operations: MAXIMUM, MINIMUM, SUCCESSOR, PREDECESSOR in $O(lgn)$ time, since its worst case is $O(h) = O(lgn)$.  

## 13.2 Rotations
The operations INSERT, DELETE might change the structure of the tree, so we have to change the color of some nodes and change pointer structure using ROTATE to keep the properties of the red black tree unviolent:  

![Tree rotate](rotate.PNG)  

There are 2 kinds of rotate:  
```
LEFT-ROTATE(T, x)
y = x.right // set y
x.right = y.left // turn y's left subtree into x's right subtree
if y.left != T.nil:
    y.left.p = x
y.p = x.p // link x's parent to y   
if x.p = T.nil:
    T.root = y.p
else if x = x.p.left:
    x.p.left = y
else
    x.p.right = y
y.left = x // put y on x's left
x.p = y
```  
and
```
RIGHT-ROTATE(T,x)
y = x.p
y.left = x.right // turn x's right tree into y's left tree
if x.right != T.nil: // if it's not leaf
    x.right.p = y
x.p = y.p
if y.p = T.nil: // if its root
    T.root = x
else if y.p.left == y:
    y.p.left = x
else:
    y.p.right = x
x.right = y
y.p = x
```

ROTATE runs in $O(1)$. Only pointers are changed, every other attributes are the same.  

# 13.3 INSERTION
Runs in $O(lgn)$ time. We insert the node z as if in binary search tree (a little modified), then color z ***red***. To guarantee the red-black properties, we use the auxilarry function RB-INSERT-FIXUP and  perform rotations.  
```
RB-INSERT(T,z)
y = T.nil
x = T.root
while x != T.nil:
    y = x
    if z.key < x.key:
        x = x.left
    else:
        x = x.right
z.p = y
if y = T.nil:
    T.root = z
if z.key < y.key:
    y.left = z
else:
    y.right = z
z.left = T.nil  
z.right = T.nil
z.color = red
RB-INSERT-FIXUP(T,z) // fix the red-black properties of the tree
```

```
RB-INSERT-FIXUP(T,z)
while z.p.color == RED:
    if z.p == z.p.p.left:
        y = z.p.p.right
        if y.color == RED:
            z.p.color = BLACK // case 1
            y.color = BLACK // case 1
            z = z.p.p
        else:
            if z == z.p.right:
                z = z.p             // case 2
                LEFR-ROTATE(T, z)   // case 2
            z.p.color = BLACK       // case 3
            z.p.p.color = RED       // case 3
            RIGHT-ROTATE(T, z.p.p)
    else: (same as before, but replace left with right, right with left)    
T.root.color = BLACK
```

First, we should note a few things: Before the RB-INSERT-FIXUP:
1. Before the insertion, no rules are violated.
2. z's child after insertion, before RB-INSERT-FIXUP is called, has 2 black leaves as child.
2. Since z has both of its children is T.nil which is BLACK, property 1 and 3 hold.
3. z replace one leaf (black), and add 2 more black leaves. Since z is red, inserting z does not change the black height of any node, therefore does not violate property 5.
4. Only 2 and 4 can be violated.

We shall now examine the ***Initialization*** and the ***Termination*** arguments:  

***Initialization*** *(Before RB-INSERT-FIXUP)*  
1. z is the red node we added.
2. At most one of the 2 and 4 properties being violated, but not both: 
    * If 2 is violated, then z must be the root. Therefore, 4 is not violated.
    * If 4 is violated, then the root color did not change. Therefore, 2 is not violated.  

***Termination*** *(After the loop)*
The loop stop when z.p is black. At that moment, z even if z is red, rules 4 is not violated anymore. So the possibly violated property is 2 which we fix by setting `T.root.color = black` at the end of the procedure.  

***Maintainence*** *(The loop)*
This loop is to maintain every properties of the tree. There are 6 cases in total, but 3 of them is symmetric to each other: whether the parent z.p is the left or right child of the grandparent z.p.p.  
First, since z.p is red (or else loop stops), z.p can't be root, which means z.p.p exists.  
Case 1 differs from case 2, 3 in the color of the uncle, the parent's "sibling" : `y = z.p.p`, the other child of the grandparent.  

**Case 1: z.p's siblings is red**  
This case is simple: 
* To avoid violating property 4, we will paint z.p black, along with him his sibling to black too. 
* However, that might ruins property 5 since it change the black-height of the above nodes, so we color the grandparent z.p.p red.  

![Case 1](case_1_insertion.PNG)

At this point, z no longer violates property 4. However, z.p.p might violates 2 and 4, so we move the z pointer to point to z.p.p. Now, z.p.p becomes new z which we will call z':
* If z' is root, then loop stops, property 4 is not violated. However, 2 is violated because z' is red. So we fix by the last line `T.root.color = black`.
* If z' is not root, 2 is not violated. 4 is possible to be violated, so we repeat the loops.  

**Case 2: z's uncle y is black and z is a right child**  
**Case 3: z's uncle y is black and z is a left child**  
Zero, note a few things:
* Look at casee 2 in the image: $\alpha$, $\beta$, $\gamma$ is black because their parents are red. $\delta$ (y) is also black because of the condition. 
* z.p.p (C) is black, because only z color is changed in the previous process, and z.p is red, therefore z.p.p must be black.

First, if the current state is case 2, we will transform case 2 into case 3:  
* Set z to its parent z.p `z = z.p` and left-rotate z. 
* After the rotation, property 3 still holds because $\alpha$, $\beta$, $\gamma$ are all black.
* Property 1 still holds too.
* Before this, bh(A) = bh(y). bh(B) = bh(A) since B is red, so bh(B) = bh(y).
* Now z (the old parent) is the left child of the old z.

Now we are in case 3:
* Since we are still violating property 4 with z and z.p (B), we wil fix it by make z.p (B) black. 
* That makes the black-height of z.p.p (C) unbalanced, hence 5 is violated. We will fix it by make z.p.p (C) red, and right-rotate z.p.p (C). (look at the image):
    - Property 2 still holds, since if B is root after right-rotation, then its black.
    - Property 3 and 4 hold, because $\gamma$ and $\delta$ is black so C does not violates it. 
    - Property 5 holds, since we plus one black (B) and minus one black (C) so the upper black-height tree is stil balanced. In addtion, C is red, so bh(A) = bh(y) = bh(C).

![Case 2 and 3](case_2_3_insertion.PNG)

***Analysis***  
The INSERTION takes $O(lgn)$ time. The RB-INSERTION-FIXUP moves the pointer 2 levels each interation, and only use at most 2 rotation, so the time for RB-INSERTION-FIXUP takes $O(lgn)$. Total, $O(lgn)$.

# 13.4 DELETION
