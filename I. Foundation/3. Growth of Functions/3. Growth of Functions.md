# 3. Growth of Functions
*Advise anyone who do not know any idea about the Big O and stuff to read the Chapter 2 first* 

When we look at the input sizes large enough to make the order of the growth of the running time relevant, we are studying the ***asymtotic*** efficiency of the algorithms.

## 3.1 Asymtotic notation
### *Asymtotic notation, functions and running times*
Asymtotic notation originally applies to **functions**, but we will use them primarily to describe the running time of the algorithms. We can also use them to describe other aspects of the algorithm such as memory space usage,...

### *$\Theta$-notation* - Theta-notation
The chapter 2 refers the worst-case running time of the insertion sort as:

$$ T(n) = \Theta(n^2) $$

Let define what this notation mean:

$$ \Theta(g(n)) = \{ f(n): \text{there exist positive constanst }  c_1, c_2, n_0 \text{such that } 0 \leq c_1g(n) \leq f(n) \leq c_2g(n) \text{for all } n \geq n_0 \} $$

A function $f(n)$ belongs to the set $\Theta(g(n))$ if there exists constant $c_1, c_2$ such that $f(n)$ *is sandwiched* between $c_1g(n)$ and $c_2g(n)$. 

Since $\Theta(g(n))$ is a set, we can write $f(n) \in \Theta(g(n))$. But since now we will cheat and write it as $f(n) = \Theta(g(n))$.

Visualization:

<img src ="fn Theta gn.PNG" alt="cant display">

As you can see in the graph, with all $n \geq n_0$, $f(n)$ is **sandwiched**. For such $f(n)$, we say that $g(n)$ is **asymtotically bound tight** for $f(n)$.

According to the definition, $f(n)$ are **asymtotically nonnegative**, which means $f(n)$ is *nonnegative* with suffiently large enough $n$. Consequently, $g(n)$ itself is also asymtotically nonnegative. And we can assume that every *functions* in $\Theta(g(n))$ is asymtotically negative as well. This holds true for other asymtotic notation later on as well.

Intutitively, we can throw away the low-order terms and ignoring the leading efficients of the highest-order terms to get the informal notion of $\Theta$-notation. For example:

$$\frac{1}{2}n^2 - 3n = \Theta(n^2)$$

In general, for any polynomial $p(n)=\sum_{i=1}^{d} a_in^i$, where $a_i$ are constants and $a_d > 0$, we have $p(n) = \Theta(n^d)$.

### $O$-notation - Big Oh notation
The $\Theta$-notation asymtomically bound a function from *above* and *below*. When we have only **asymtomically uppger bound**, we use $O$-notation.

With kind-of a similar definition, we have the definition of $O(g(n))$:

$O(g(n)) =$ $\{f(n):$ there exists positive constants $c$ and $n_0$ such that $0 \leq f(n) \leq c g(n)$ for all $n \geq n_0$ $\}$

Visualization:

<img src="fn big oh gn.PNG">

Likewise, we also write $f(n) = O(g(n))$ to indicate that $f(n) \in O(g(n))$. We can see that if $f(n) = \Theta(g(n))$ then $f(n) = O(g(n))$. In the other words, $\Theta(g(n)) \subseteq O(g(n))$.

As an unique example, any *linear* function $an + b$ is in $O(n^2)$

Using the $O$-notation, we can describe the running time of an algorithm merely by inspecting the algorithm's overall structure.

Sicne $O$-notation describes an upper bound, when we use it to bound the worst-case running time of the algorithm, we have a bound on the running time of the algorithm on every input. When we say *"the running time is $O(n^2)$"*, we mean that there is a function $f(n) = O(n^2)$ such that for any value of $n$ ,no matter what particular input size $n$ is chosen, the running time of input is bounded from above by $f(n)$. 

### *$\Omega$-notation* - Omega notation
Just as $O$-notation provides an *asymptotic upper bound* for the function, $\Omega$-notation provides an *asymptotic lower bound*. Similar to the $\Theta$-notation and the $O$-notation, the $\Omega(g(n))$ is defined as the following:

$\Omega(g(n))$ = $\{f(n):$ there exists positive constants $c$, $n_0$ such that $0 \leq cg(n) \leq f(n)$ for all $n \geq n_0 \}$

Visualization:

<img src="fn big omega gn.PNG" alt="Toang">

As an intuition, after $n \geq n_0$, $f(n)$ will never get below $g(n)$

#### Theorem 3.1
For any two functions $g(n)$ and $f(n)$, we have $f(n) = \Theta(g(n))$ if and only if $f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$

We usually use the above theorem for proving the tight bounds using the upper bound and lower bound than reverse.

When we say the *running time* is $\Omega(g(n))$ we mean no matter what the input size of n is **the running time of input is at least a constant times of g(n)**, for sufficiently large *n*. In other words, we are giving a lower bound on the best-case running time of an algorithm. For example, the best-case running time of insertion sort is $\Omega(g(n))$. Therefore, the insertion sort belongs to both $\Omega(n)$ and $O(n^2)$.

### **Asymptotic notation in equations and inequalities**
In general, when asymptotic notation appears in a formula, we interpret it as standing for some annonymous functions that we do not care to name. For example:
 $2n^2 + 3n + 1 = 2n^2 + \Theta(n)$
means that $2n^2 + 3n + 1 = 2n^2 + f(n)$ where $f(n) = \Theta(n)$, in particular $f(n) = 3n + 1$. Using asymptotic notation in this manner can help *eliminate inessentnial detail and clutter in an equation*.

The number of annonymous equation in an expression is undertood to be equal to the number of times the asymptotic notation appears. For example: $\sum_{i=1}^{n}O(i)$ there is only *a single* annonymous function: $O(i)$.

### $o$-notation
The asymptotic upper bound provided by $O$-notation may or may not be asymptotically tight. For example, $2n^2 = O(n^2)$ is asymptotically tight but $2n = O(n^2)$ is not. So to denote an upper bound that is not asymptotically tight, we use $o$-notation - call *"little oh of $g(n)$"*, defined as following:

$o(g(n)) = \{f(n):$ for any positive constants $c > 0$, there exists a constant $n_0$ so that $0 \leq f(n) < cg(n)$ for all $n \geq n_0 \}$

For example, $2n = o(n^2)$ but $2n \neq o(n)$. Intuitively, there always exists the constant $n_0$ so that no matter how small the constant $c$ is, $cg(n)$ still outscales $f(n)$ with suffiently large enough $n \geq n_0$. In another math word:


$$\lim_{n \to \infty} {\frac{f(n)}{g(n)}} = 0$$

### $\omega$-notation - little omega notation
Likewise, we use $\omega$-notation to denote a lower bound that is not asymptotically tight. One way to define it is:

$f(n) = \omega(g(n))$ if and only if $g(n) = o(g(n))$

Likewise, obviously it will have a similar definition as the $o$-notation:

$\omega(g(n)) = \{f(n):$ for any positive constants $c > 0$, there exists a constant $n_0 > 0$ such that $0 \geq cg(n) < f(n)$ for all $n \geq n_0\}$ 

### Comparing functions

Many of the relational properties of real number applies to asymptotic comparisons as well:

**Transivity** (page *51*)

$f(n) = \Theta(g(n))$ and $g(n) = \Theta(h(n))$ imply $f(n) = \Theta(h(n))$

$f(n) = O(g(n))$ and $g(n) = O(h(n))$ imply $f(n) = O(h(n))$

$f(n) = \Omega(g(n))$ and $g(n) = \Omega(h(n))$ imply $f(n) = \Omega(h(n))$

$f(n) = o(g(n))$ and $g(n) = o(h(n))$ imply $f(n) = o(h(n))$

$f(n) = \omega(g(n))$ and $g(n) = \omega(h(n))$ imply $f(n) = \omega(h(n))$

**Reflexivity**

$f(n) = \Theta(g(n))$

$f(n) = O(g(n))$

$f(n) = \Omega(g(n))$

**Symmetry**

$f(n) = \Theta(g(n))$ if and only if $g(n) = \Theta(f(n))$

**Transpose symmetry**

$f(n) = O(g(n))$ if and only if $g(n) = \Omega(g(n))$

$f(n) = o(g(n))$ if and only if $g(n) = \omega(g(n))$

We say that $f(n)$ is asymptotically smaller than $g(n)$ if $f(n) = o(g(n))$, and $f(n)$ is asymptotically larger than $g(n)$ if $g(n) = \omega(g(n))$.

### **Exercises**
***3.1-1***

*Hint*: assume that $g(n) \geq f(n)$ will solve it.

***3.1-2***

*Hint*: using **inductive proof**.

***3.1-4***

Simple math $2^{n+1} = 2\times2^n = O(n)$

Assume that $m = 2^n$ then $2^{2n} = m^2$. As a result, $m^2=O(m)$ does not seem legit.

***3.1-5***

#### Theorem 3.1
For any two functions $g(n)$ and $f(n)$, we have $f(n) = \Theta(g(n))$ if and only if $f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$

*(1) If we have $f(n) = \Theta(g(n))$, we have $f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$*

From $f(n) = \Theta(g(n))$, there exists a positive constant $c_1$ so that $0 \leq c_1g(n) \leq f(n)$ with sufficiently large enough $n$. As a result, $f(n) = O(g(n))$. Likewise, we can prove $f(n) = \Omega(g(n))$

*(2) We have $f(n) = \Theta(g(n))$ if we have $f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$*

$f(n) = O(g(n))$ and $f(n) = \Omega(g(n))$ $\Rightarrow$ There exists a positive constant $c_1, c_2$ so that $0 \leq c_1g(n) \leq f(n) \leq c_2g(n)$ with sufficiently large enough $n$. As a result, $f(n) = \Theta(g(n))$.

***3.1-6***

Using theorem 3.1.

***3.1-7***

Assume that there exists a function $f(n)$ that $f(n) = o(g(n))$ and $f(n) = \omega(g(n))$. 
Since $f(n) = o(g(n))$ for any positive constants $c$ there is always $n_0$ suffiently large enough so that $f(n) < cg(n)$ for all $n \geq n_0$. Take a random positive constant $c'$ the condition still applies. However, since $f(n) = \omega(g(n))$, for any positive constant $c$ including $c'$ $f(n) > c'g(n)$ which is contradictory. So our inital assumption is wrong.

## 3.2 Standard notations and common functions
### ***Monocity***
A function $f(n)$ is *monotically increasing* if $m \leq n$ implies $f(m) \leq f(n)$. Similiarly, $f(n)$ is *monotically decreasing* if $m \leq n$ implies that $f(m) \geq f(n)$.

A function $f(n)$ is *strictly increasing$ if $m < n$ implies that $f(m) < f(n)$. Similarly, $f(n)$ is *strictly decreasing* if $m < n$ implies that $f(m) > f(n)$.

### ***Floors and ceilings***
$\lfloor x \rfloor$ : the greatest interger less than or equal to $x$

$\lceil x \rceil$ : the least integer greater or equall to $x$

For any integer $n$:

$$\lfloor x \rfloor + \lceil x \rceil = n$$

For any real nunmber $x \geq 0$ and integers $a, b > 0$:

$$\Bigl\lfloor \frac{\lfloor xa \rfloor}{b} \Bigr\rfloor = \Bigl\lfloor \frac{x}{ab} \Bigr\rfloor$$

<br>

$$\Bigl\lceil \frac{\lceil xa \rceil}{b} \Bigr\rceil = \Bigl\lceil \frac{x}{ab} \Bigr\rceil$$

<br>

$$\Bigl\lfloor \frac{a}{b} \Bigr\rfloor \geq \frac{a - (b - 1)}{b} $$

<br> 

$$\Bigl\lceil \frac{a}{b} \Bigr\rceil \leq \frac{a + (b - 1)}{b} $$

### ***Modular arithmetic***
*To keep this document short, I will not go in this part since it is grade-7 knowledge.*

### ***Polynomials***
For a nonnegative $d$, a **polynomial** $p(n)$ is a function of the form: $\sum_{i=0}^{d} {a_in^i}$ where the constants $a_0, a_1, ..., a_d$ are **coefficients** of the polynomial and $a_d \neq 0$.

A polynomial is asymptotically positive if and only if $a_d > 0$. For an asymptotically postive polynomial $p(n)$ of degree $d$, we have $p(n) = \Theta(n^d)$. 

For any real constant $a$:
- $a > 0 \Rightarrow n^a$ is *monotically increasing*
- $a < 0 \Rightarrow n^a$ is *monotically decreasing*  

We say that function $f(n)$ is **polynomial bounded** if for some constant $k$ : $f(n) = O(n^k)$

### ***Exponentials***

For all real constants $a, b$ such that $a > 1$ we have:

$$lim_{n \to \infty} \frac{n^b}{a^n} = 0$$

from which we can conclude that: $n^b = o(a^n)$. Thus, any exponential function with base greater than 1 grows faster than any polynomial function.

With $e$ as the base natural algorithm function, for all real $x$:

$$e^x = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + ... = \sum_{i=0}^{\infty} \frac{x^i}{i!}$$

For all real $x$, we have the inequality, where euqality holds only when $x = 0$:

$$ e^x = x + 1$$

When $|x| \leq 1$, we have the approximation:

$$ 1 + x \leq e^x \leq 1 + x + x^2$$

When $x \to 0$, the approximation of $e^x$ is quite good:

$$ e^x = 1 + x + \Theta(x^2)$$

And we have for all $x$:

$$lim_{n \to \infty} \left({1 + \frac{x}{n}}\right)^n = e^x$$

### ***Logarithms***
We are going to use the following notation:

$lg(n) = log_{2}n$ (binary logarithm)

$ln(n) = log_{e}n$ (natural algorithm)

$lg^k{n} = (lg{n})^k$ (exponentiation)

$lg{lg{n}} = lg(lgn)$ (composition)

if we hold $b > 1$ constant, the function $log^b{n}$ is **strictly increasing**.

There is a simple series expansion for $ln(n + 1)$ when $|x| < 1$:

$$ln(x + 1) = x - \frac{x^2}{2} + \frac{x^3}{3} - \frac{x^4}{4} + \frac{x^5}{5} ...$$

For $x > -1$, we also have the following inequalities:

$$\frac{x}{x+1} \leq ln(1+x) \leq x$$

where inequalities hold for $x = 0$.

We say function $f(n)$ is **polylogarithmically bounded** if $f(n) = O(lg^k{n})$ for some constant $k$.

With equation 

$$lim_{n \to \infty} \frac{n^b}{a^n}$$

we can replace $n$ with $lgn$ and $2^a$ for $a$ and we have:

$$lim_{n \to \infty} \frac{lg^b{n}}{(2^a)^{lgn}} = lim_{n \to \infty} \frac{lg^b{n}}{n^a} = 0$$

The above equation yields that the *polinomial function* grows faster than the *polylogrithmically function*. So, for any constant $a$:

$$lg^b{n} = o(n^a)$$

### ***Factorials***
A weak upper bound of factorials is that $n! \leq n^n$ which equality holds then $n = 1$

We have the ***Stirling's approximation***:

$$n! = \sqrt{2\pi n} \Bigl(\frac{n}{e}\Bigr)^n \Bigl(1 + \Theta\Bigl(\frac{1}{n}\Bigr)\Bigr)$$

gives us a tighter upper bound, and a lower bound as well.

We also have:

$$n! = o(n^n)$$

$$n! = \omega(2^n)$$

$$lg(n!) = \Theta(nlgn)$$

The following equation holds for $n \geq 1$

$$n! = \sqrt{2\pi n}\Bigl(\frac{n}{e}\Bigr)^n e^{\alpha n}$$

where

$$\frac{1}{12n + 1} < \alpha_n < \frac{1}{12n}$$

### ***Functional interation***
We use the notation $f^{(i)}(n)$ to denote that the function $f(n)$ is applied $i$ times to an initial value of $n$. For non-negative integer $i$, we recursively define

$$f^{(i)}(n) = 
    \begin{cases}
      n, & \text{if}\ i = 0 \\
      f(f^{(i-1)}(n)), & \text{if}\ i > 0
    \end{cases} $$

For example, if $f(n) = 2n$ then $f^{(i)} (n) = 2^i n$

### ***The iterated logarithm function***
We use the notation $lg^{*}n$ (*"the log start of $n$"*) to denote the iterated logarithm, defined as follow:

$$lg^{*}n = min\{i \geq 0: lg^{(i)}n \leq 1\}$$

The iterated logarithm is a very slowly growing function:

$lg^{*}2 = 1$

$lg^{*}4 = 2$

$lg^{*}16 = 3$

$lg^{*}65536 = 4$

$lg^{*}2^{65536} = 5$

### ***Fibonacci numbers***
If you do not know what are Fibonacci numbers, search Google first.
Fibonacci numbers are related to the **golden ratio* $\phi$ and to its conjugate $\overline{\phi}$ which are the roots of the following function

$$x^2 + x +1 = 0$$

and are given by the folling formula:

$$\phi = \frac{1 + \sqrt5}{2}$$

$$\overline{\phi} = \frac{1 - \sqrt5}{2}$$

Specifically, we have:

$$F_i = \frac{\phi^i - \overline{\phi}^i}{\sqrt5}$$

Since $\frac{|\overline{\phi^i}|}{\sqrt5} < \frac{1}{\sqrt5} < \frac{1}{2}$ which implies that

$$F_i = \Bigl\lfloor \frac{\phi^i}{\sqrt5} + \frac{1}{2} \Bigr\rfloor$$

