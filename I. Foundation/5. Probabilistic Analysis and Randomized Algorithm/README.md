*Advise you to read Appendix C before reading this*  
*And this has lots of text*

# 5. Probabilistic Analysis and Randomized Algorithm
## 5.1 Hiring problem
Problem statement:
> Suppose you are trying to hire a new office assitant through an employment agency. They send you one candidate each day. You interview that person to decide whether to hire him/her or not. However, if you decide to hire him, the cost would be much better because you have to pay fee to fire the current job assistant. Since you always need as the best office assistant as possible, if the person you interview is better than the current office assistant, you will immediately hire the person. You are ready to pay any price for the best office assistant but want to estimate the price.  

The following pseudo-code describe the problem:  
*HIRE-ASSISTANT*
``` 
best = 0
for i from 1 to n:
    interview candidate i
    if candidate i is better than candidate best:
        best = i
        hire candidate i
```

Different from Chapter 2 in which we focus on the running time of the algorithm, now we focus on the price we pay to hire the new office assistant. However, they are similar, since here we are counting a number of times ***an operation is executed***.  
Assume the interviewing cost is $c_i$, the hiring cost is $c_h$, the total people we interview is $n$ while the number we decided to hire is $m$. Then the hiring cost is $O(c_in + c_hm)$. No matter how many people we hire, we always have to interview the whole $n$ people, so the cost $c_in$ is constant. Therefore we only concentrate on the cost $c_hm$. This quantity varies with each run.  
### **Worst-case analysis**
The worst case is when we have a sequence of strictly increasingly order of skill value. Then, we have to hire everyone we interview, so the hiring cost is $O(c_hn)$. However, the worst-case do not always happen, therefore we want to consider the average-case.  
### **Probabilistic analysis**
***Probabilistic analysis*** is the use of probability in analysis. In order to perform probabilistic analysis, we must use the knowledge of, or make assumption about, the distribution of the input.  Then we analyze the algorithm, computing the average-case running time, in another word - we take the average over the distribution of all possible inputs. This we refer to as ***average running-time***.  
We must be careful in deciding the distribution of inputs. For some problems, we must *assume* something about the set of all possible inputs, then we can use probabilistic analysis to find an efficient algorithm or as a means to gain *insight* into the problem. If we cannot describe the *reasonable* input distribution, we cannot use probabilistic analysis.  
For the hiring problem, we can assume that applicants come in random order. Assuming that we can compare and qualify who is a better assistant, we rank every candidates with a unique number from *1* to *n*, with ```rank(i)``` to denote the rank of applicant $i$. Therefore, the order list $\{rank(1), rank(2), ... rank(n)\}$ is a permutation of $\{1, 2, ..., n\}. Saying random order is equivalent to say the `rank` order can be any of the $n!$ permutations possible. Alternatively, we can say the ranks form a ***uniform random permutation*** - which any of the $n!$ permutation has an equal likelihood to happen.

#### **Randomized algorithm**
If we know about the distribution of input, we can use the probability and randomness as a tool for algorithm design, by making part of the behaviour of the algorithm random.   
In the hiring problem, it seems that the candidates come  to us in random order, however we do not really know whether that is true. Therefore, we change the model slightly: we assume the agency beforehand sent us a list of candidates, and each day we choose a person from it to interview. Thus, we have enforced a ***random order***.   
Moreover, we call an algorithm **randomized** if its behavious is not only controlled by its inputs but also a ***random-number generator***.  
We will discuss the **average-case running time** when the probability distribution is over the inputs to the algorithm, and we discuss the **expect running time** when the algorithm itself make a random choices. In this hiring problem case, we are discussing the **expected running time**.  

## 5.2 Indicator random variables
Indicator random variables are used in many problems, including this. Suppose we are given a smaple space $S$ and an event $A$. The the ***indicator random variable*** *(Reading Appendix C.3 for the definition of "random variable")* $I\{A\}$ is defined as:  

$$I\{A\} = \begin{cases}
1 & \text{if A occurs}\\
0 & \text{if A does not occur}
\end{cases}$$

Simple example, we shall count the number of heads when we flip a fair coin. Our sample space is $S=\{H, T\}$, with $Pr\{H\} = Pr\{T\} = 1/2$. Define an indicator random variable $X_H$ associated with event H - heads. This varible count the number of times the coin come up head:

$$X_H = I\{H\} = \begin{cases}
1 & \text{if event H}\\
0 & \text{if event T}
\end{cases}$$

Then the expected number of coin flipping head is simply expected value of our indicator random variabe:

$$E[X_H] = E[I\{H\}] = 1 \times 1/2 + 0 \times 1/2 = 1/2$$

The expected value of the indicator random variable of event A is also the probability that event A occurs, as the following **lemma** shows:  
***Lemma 5.1***
Given a sample space S and an event A in sameple space S. Let $X_A = I\{A\}$ then $E[X_A] = Pr\{A\}$.  
**Proof**

$$E[X_A] = E[I\{A\}] = 1 \times Pr\{A\} + 0 \times Pr\{\overline{A}\} = Pr\{A\}$$

Indicator random variables are useful for analyzing situations in which we performed repeated random trials. Assume we flip a fair coin $n$ times, and want to calculate the number of heads. We can define $X_i = I\{\text{the }i\text{th results in H}\}$ and $X$ is the number of totals heads in the $n$ coin flips. We have:  
$X = \sum_{i=1}^{n}X_i$  
We calculate the expected number of heads, so we have:

$E[X] = E[\sum_{i=1}^{n}X_i]$  
______$= \sum_{i=1}^{n}E[X_i]$ (by *C.21*)  
______$= \sum_{i=1}^{n}1/2$ (by *lemma 5.1)  
______$= n/2$  
(*Sorry dont have a better way of indent*)

**Analysis of the hiring problem using the indicator random variables**
In order to use probabilistic analysis, we assume that the candidates come in random order. To calculate the expected number of hiring times, we define $X$ as the random variable whose values equals the number of times we hire a new office assistant. Then the expected hiring times is:

$$E[X] = \sum_{x=1}^{n}x Pr\{X = x\}$$

But this is cumbersome. Instead we use indicator random variable. Instead of one variable associated with the number of times we hire a new assistant, we define $n$ variables related to whether or not a candidate is hired. Particular we have:

$$X_i = I\{\text{candidate }i\text{ is hired}\}
= \begin{cases}
1 & \text{if candidate i is hired}\\
0 & \text{if candidate i is not hired}\\
\end{cases}$$

and 

$$X = X_1 + X_2 + ... + X_n$$  
By Lemma 5.1 we have:

$$E[X_i]=Pr\{\text{candidate }i\text{ is hired}\}$$
To be hired, the $i$th candidate has to be the best qualifed among the first $i$ candidate. Because we are assuming inputs come in random order, the first $i$ candidates also come in random order, which mean they are euqally likely to be the best among the first $i$. Therefore:  

$$E[X_i] = Pr{X_i} = 1/i$$
Therefore:  
$E[X] = E[\sum_{i=1}^{n}X_i]$  
______$=\sum_{i=1}^{n}E[X_i]$ (by linearity of expectation)  
______$=\sum_{i=1}^{n}1/i$  
______$=ln{n} + O(1)$  (by equation *A.7*)

As a result, even though we interview $n$ people, eventually we hire averagely $lnn$ people.

**Lemma 5.2**
Assuming the candidates are in random order, algorithm has an average-case total hiring cost of $O(c_hlnn)$

## 5.3 Randomized algorithm
In problems such as the hiring assistant problem, it is very useful to assume the random order of the candidates. Now instead of assumption, we will force the randomness on the input data by randomize it ourself - in another word permute the input before dealing with it. In this way, the randomness of the input is ensured no matter the input, and we still got the $lnn$ running time. Note that the randomness does not lie in the input, but in the algorithm. In this way, even your worst enemy - which means worst-case scenario cannot force an bad input array, since the permutation makes the input order irrelevant.  
Now we will update the pseudo-code of the hiring assistant problem:
```
randomly permute the list of candidates
best = 0
for i = 1 to n:
    interview candidate i
    if candidate i is better than candidate best:
        best = i
        hire i
```
After randomizing the input, we achieve a similar situation to the one which we make assumption about the input random order. Therefore we have:  

**Lemma 5.3** 
The expected hiring cost of the above algorithm is $O(c_hlnn)$  

Note that in the Lemma 5.2, we assume that the input is in random order while in the Lemma 5.3 we actively randomzing the array. To remain consistent with the terminology, we couched Lemma 5.2 in terms of average-case hring cost while Lemma 5.3 in terms of expected hiring cost.  

**Randomly permuted array**  
Many algorithms involes the randomization of the input array. Here we discuss 2 methods to do so. Without losing the loss of generality, we assume the array $A$ contains elements from $1$ to $n$. Our goal is to produce the permutation of the array.  
One common method is associated one element with its own priority and sort the elements in $A$ according to the priorities. We call the procedure PERMUTE-BY-SORTING:
```
PERMUITE-BY-SORTING(A)
n = A.length
let P[1..n] be new array
for i = 1 to n:
    P[i] = RANDOM(1, n^3)
sort A, using P as sort keys
```
We use a range from $1$ to $n^3$ to make it more likely that all elements in P are unique. Let assume that all the priorities are unique. We have to prove that the procedure produces a ***uniform random permutation*** (that is it is equally likely to produce every permutation of number from $1$ to $n$)

**Lemma 5.4**  
Procedure PERMUTE-BY-SORTING produces a uniform random permutation of the input, assuming all priorities are unique.

 ***Proof***  
We start by considering the case in which the $i$th elements $A[i]$ got the $i$th smallest priority. We shal prove that this permutation occurs with exact probability of $1/n!$  
For $i = 1,2, ...,n$, call event $E_i$ is the event when $A[i]$ get the $i$th smallest priority. Then we wish to compute the probability for all $i$ that $E_i$ occurs, which is:

$$P = Pr\{E_1\cap E_2 \cap E_3\cap...\cap E_n\}$$

Using exercise *C 2.5*, this probability is equal to:

$P = Pr\{E_1\}Pr\{E_2|E-1\}Pr\{E_3|E_2 \cap E_1\}...Pr\{E_i|E_1 \cap ... E_{i-1}\}...Pr\{E_n|E_1 \cap ... E_{n-1}\}$

We have that $Pr\{E-1\}$ is equal to $1/n$ because the probability one priority is choosen random the smallest probability is $1/n$. The $Pr\{E-2|E_1\} = 1/{n-1}$
because the probability to choose the 2nd smallest probability among the left $n-1$ elements is $1/n - 1. Like wise, we have $Pr\{E_i|E_1 \cap E_2 \cap ... \cap E_{i-1}\} = 1/{n - (i-1)}$ since the probability for the $i$th elemnt to have the $i$th smallest probability among the left $n - (i-1)$ is $1/{(n-(i-1))}$ . Thus:

$$P = \Bigl(\frac{1}{n}\Bigr) \Bigl(\frac{1}{n - 1}\Bigr)  ... \Bigl(\frac{1}{2}\Bigr) \Bigl(\frac{1}{1}\Bigr) = \frac{1}{n!}$$

We can extend the above proof for any permutation. Assume the priorities are $p[1], p[2], ... p[n]$ accordingly for $n$ elements in $A$. For the $i$th elemtns $A[i]$, the probability for it to get the $p[i]$ th priority among the left $n - (i - 1)$ is still $1/{(n - (i - 1))}$, doesn't change. Therefore for any particular permutation, the probability is still $1/n!$.

It is insufficient to just show that the probability that an elemetns $A[i]$ winds up in the position $j$ is $1/n$.

A better way for generating a random permutation is to permute the given array in place. The procedure RANDOMIZE-IN-PLACE do so in $O(n)$ time. In the $i$th iteration, we choose a random $j$ from $i$ to $n$ and swap $A[i]$ with $A[j]$:
```
RANDOMIZE-IN-PLACE(A)
n = A.length
for i = 1 to n:
    swap A[i] with A[RANDOM(i,n)]
```
**Lemma 5.5**
Procedure RANDOMIZE-IN-PLACE computes a uniform random permutation.  

We can prove this with inductive method.

## 5.4 Probabilistic and further uses of indicator random variables
### 5.4.1 The birthday paradox
Problem statement:
> How many people must there be in the room before there is 50% chance that two of them were born in the same day?
We can analyze the probability of 2 or more people have matching birthday by considering the complemnetary event of it. The probability of 2 or more people have matching birthday is 1 minus the probability of everyone has distinct birthday.  

Assume the year has $n = 365$ days, the days are uniformly distrubuted.  
Calling $A_i$ the event that the person $i$ has distinct birthday from all $j$ with $j$ from $1$ to $i$, $B_k$ the event that the people $1,2,...,k$ have different birthday. We have:

$$B_k = \bigcap_{i=1}^{k} A_i$$

To have $k$ distinct birthday people, we must first have $k-1$ distinct birthday people, adding a $k$th people to the list who is different birthday from all the people from $1, 2, ..., k - 1$. Therefore:

$$Pr\{B_k\} = Pr\{B_{k-1}\} Pr\{A_k|B_{k-1}\}$$ (5.7)

We have $Pr\{A_k|B{k-1}\} = (n - k +1)/n$ because after choosing $(k-1)$ days, we have $n - k + 1$ days left, therefore the chance to get a different day from the first $k-1$ days is $1/(n - k + 1)$. We iteratively apply the recurrences (5.7) we have:  
$Pr\{B_k\} = Pr\{B_{k-1}\}Pr\{A_k|B_{k-1}\}$  
_________$=Pr\{B_{k-2}\}Pr\{A_{k-1}|B_{k-2}\}Pr\{A_{k}|B_{k-1}\}$  
_________$=... = Pr\{B_1\}Pr\{A_2|B_1\}...Pr\{A_{k-1}|B_{k-2}\}Pr\{A_{k}|B_{k-1}\}$  
_________$= 1 \cdot \Bigl(\frac{n -1}{n}\Bigr) \cdot \Bigl(\frac{n - 2}{n}\Bigr) ... \cdot \Bigl(\frac{n - k + 1}{n}\Bigr)$  
_________$=\Bigl(1 - \frac{1}{n}\Bigr) \cdot \Bigl(1 - \frac{2}{n}\Bigr) ... \cdot \Bigl( 1- \frac{k-1}{n}\Bigr)$

According to inequality (3.12), we have $1 + x \leq e^x$, therefore:  
$Pr\{B_k\} \leq e^{-1/n} \cdot e^{-2/n} \cdot ... \cdot e^{(-k+1)/n}$  
_________$= e^{-\sum_{i=1}^{k-1}i/n}$  
_________$=e^{-k(k-1)/2n}$  
_________$\leq 1/2$

Therefore, $-k(k-1)/2n \leq ln{1/2}$. Solving it we have: $k \geq (1 + \sqrt{1 + (8 + ln{2})n})/2$. For $n = 365$, we have $k \geq 23$. So the least people we need in a room is 23.

### **An analysis using indicator random variables**  
This one is much simpler. Suppose that birthday are independent. Thus the probability to have 2 people on the same day $r$ is:  

$Pr\{b_i=r \text{ and } b_j = r\} = Pr\{b_i = r\}Pr\{b_j=r\}$  
________________________$=1/n \cdot 1/n = 1/n^2$

Thus, the probability for 2 people to have same birthday is:
$Pr\{b_i = b_j\} = \sum_{r=1}^{n}Pr\{b_i=r \text{ and } b_j = r\}$  
_____________$=\sum_{r=1}^{n}1/n ^ 2$  
_____________$=1/n$  

For each pair $(i,j)$ of $k$ in the room, we define an indicator random variable $X_{ij}$ that:

$$X_{ij} = I\{\text{person }i \text{ and } j \text{has the same birthday}\} = \begin{cases}
1 & \text{if person i and j has the same birthday} \\
0 & \text{if not}
\end{cases}$$

By Lemma 5.1 and above equation, we have:

$$E[X_ij] = Pr\{\text{person i and j has the same birthday}\} = 1/n$$ 

Let X be the random variables that count the number of pairs of same birthday in a year, we have:  
$E[X] = E[\sum_{i=1}^{k}\sum_{j=i}^{k}X_{ij}]$  

______$=\sum_{i=1}^{k}\sum_{j=i}^{k}E[X_{ij}]$  

______$=\Bigl(\frac{k}{2}\Bigr) 1/n = \frac{k(k-1)}{2n}$

Therefore, $\frac{k(k-1)}{2n} \geq 1$ since there needs to be at least 1 pair of the same birthday. Thus, if there are at least $\sqrt{2n}+ 1$ people in a room, there will 50% chance that 2 people have the same birthday. With $n=365$, we have $k \geq 28$.

Although the exact number of 2 methods differ, they are asymptotically the same: $\Theta(\sqrt{n})$.

### 5.4.2 Balls and pins
Problem statement:
> Consider a process we randomly toss identiacal balss into $b$ bins, numbered from $1, 2, ..., b$. The toss are independent, the the ball are equally likely to end up in any bins. The probability a tossed ball landed in any binbs is $1/b$.

This is a sequence of the *Bernoulli's trials (Read C.4 for more information)*. *Success* is when the ball ends up in the given bins which has the probability of $1/b$.
Here we have ourselves a list of questions:  
>*How many balls fall in a given bin?*  

 The number of balls fall in the given bin follows the **binominal distribution** $b(k;n, 1/b)$. Define *Success* as the ball tossed falls in the given bins, while *Failure* as the ball tossed falls in other bins. Then the probability for success is $p = 1/b$, while the probability is $q = 1-  1/b$. Define random variable $X$ as the number of balls falling in the given bins. Then we have:  

$$Pr\{X = k\} = {n \choose k}p^{k}q^{n - k}$$  
  
*(Choosing $k$ balls out of $n$ balls, they have the probability of $p$ to succeed while the $n-k$ rest balls have the probability of $q$ to fail.)*  
The average expected balls falling in the given bins is:

$$E[X] = \sum_{i=0}^{n} k \cdot Pr\{X=x\}=n/b$$

*(You can read C.4 for more proof, I only describe it a lil here)* 

>How many balls must we toss, on average, until the given/choosen bin contains a ball?

The number of balls we need to toss follows the **geometric distribution**. Define *success* and *failure* as above. Define random variable *X* as the number of balls we must toss before getting a ball in the given bins. We have:

$$Pr\{X=k\}=q^{k-1}p$$

*(The first $q$ toss fail, the last one succeeds)*  

The expected number of toss until success is:

$$E[X] = \sum_{i=1}^{n} Pr\{X=x\} = b$$

*(Read Appendix C.4 for more proof and details)*

>How many balls we toss until every bin contains at leat a ball?  

Let we call a ball "hit" when it is tossed into one of the empty bin. We divide the process into $n$ stages, each stage contains a sequence of tosses until a ball "hit". Thus, in the $i$th stage which means there are already $i - 1$ non-empty bins and $b - i + 1$ empty bins, the probability of a ball tossed "hit" is $(b-i+1)/b$. Define $n_i$ the random variable which counts the number of tosses needed in the $i$th stage to get a "hit" ball, $n$ the number of balls needed to finish. If we treat "hit" as *success* and "not hit" as *failure*, we can see that $n_i$ follows the *geometric distribution* - tossing until a ball "hit" with a probability of "hit" of $(b-i+1)/b$. Therefore:  
$E[n_i] = b/(b-i+1)$ (by C.32)  
And  
$E[n] = E[\sum_{i=1}^{n}n_i]$  
______$=\sum_{i=1}^{n}E[n_i]$ (by linearity of expectation)  
______$=\sum_{i=1}^{n}\frac{b}{b-i+1}$  
______$=b\sum_{i=1}^{n}\frac{1}{i}$  
______$=b(ln{b} + O(1))$ (by equation (A.7))  

So, it took approximately averagely $bln{b}$ to have every bins has at least a ball. This is also called **coupon's collector** problems.

### 5.4.3 Streaks
Problem statement:
> Suppose you flip a fair coin $n$ times. What is the longest streak of consecutive heads you expect to see?

The answer is $\Theta(lgn)$. Proof follows.  
We first prove the expected length of the longest streak of heads is $O(lgn)$. Let $A_{ik}$ be the event that the streak of at least $k$ heads begins at position $i$, with $1 \leq k \leq n$ and $1 \leq i \leq n - k + 1$. Which means that the coins at position $i, i+1, ..., i + k - 1$ yields heads. Since each coin flips are independent, we have:  
$Pr\{A_{ik}\} = 1 / 2^k$  (5.8)  
For $k = 2\lceil lgn \rceil$,  
$Pr\{A_{ik}\} = 1 / 2^{2\lceil lgn \rceil} \leq 1/2^{2lgn} =  1/n^2$   
Thus, the probability of streaks of at least $2\lceil lgn \rceil$ begins at a particular position $i$ is quite small. There are $n - 2\lceil lgn \rceil + 1$ positions like that. So the probability that a streak of at least $2\lceil lgn \rceil$ begins anywhere is:  

$Pr\{\bigcup_{i=1}^{n - 2\lceil lgn \rceil + 1}A_{ik}\} =\sum_{i=1}^{n - 2\lceil lgn \rceil + 1}1/n^2$ (by Boole's equality C.19)  
_____________________$< \sum_{i=1}^{n}1/n^2$  
_____________________$< 1/n$ (eq. 5.9)  

Call event $L_j$ is the event that the longest streak of heads has the exact length of $j$ for $j = 0,1,2,..n$. $L$ is the longest streak of heads. Then we have:  
$E[L] = \sum_{j=0}^{n}j\cdot Pr\{L_j\}$  
Intuitively, we can see that as $j$ increases, the $Pr\{L_j\}$ rapidly decrease, since the longer the streaks the harder to get it. Logically, since the events $L_j$ are disjoint, which means no 2 events among that happens at the same time, the probability that a streaks of at leat $2\lceil lgn \rceil$ happens anywhere is $\sum_{j=2\lceil lgn \rceil}^{n}Pr\{L_j\}$. Using *(5.9)*, we have $\sum_{j=2\lceil lgn \rceil}^{n}Pr\{L_j\} < 1/n$.  
Also, noting that $\sum_{j=0}^{n}Pr\{L_j\} = 1$, we have $\sum_{j=0}^{2\lceil lgn \rceil - 1}Pr\{L_j\} \leq 1$. Thus, we obtain:  
$E[L] = \sum_{j=0}^{n}j\cdot Pr\{L_j\}$  

______$= \sum_{j=0}^{2\lceil lgn \rceil - 1}j\cdot Pr\{L_j\} + \sum_{j=2\lceil lgn \rceil}^{n}j\cdot Pr\{L_j\}$  

______$< \sum_{j=0}^{2\lceil lgn \rceil - 1}2\lceil lgn \rceil \cdot Pr\{L_j\} + \sum_{j=2\lceil lgn \rceil}^{n}n\cdot Pr\{L_j\}$  

______$=2\lceil lgn \rceil\cdot \sum_{j=0}^{2\lceil lgn \rceil - 1} + n\cdot \sum_{j=2\lceil lgn \rceil}^{n}Pr\{L_j\}$  

______$< 2\lceil lgn \rceil \cdot 1 + n \cdot (1/n)$  

______$= 2\lceil lgn \rceil + 1$

______$=O(lgn)$

The probability that there is a streak exceeding $r\lceil lgn \rceil$ diminishes quiclky as $r$ increase. For $r\geq 1$, the probability that there is a streak of length at least $r\lceil lgn \rceil$ starts at position $i$ is:  
$Pr\{A_{ir\lceil lgn \rceil} \}=1/2^{r\lceil lgn \rceil} \leq 1/n^r$  
Therefore, the probability that there is at least a streak of length at least $r\lceil lgn \rceil$ is $n/n^r = 1/n^{r-1}$, or equivilently, the probability that there is a streak less than $r\lceil lgn \rceil$ length is $1 - 1/n^{r-1}$.  
As an example, suppose we have $n=1000$ coin flips. The probability that there is at least $2\lceil lgn \rceil = 20$ heads is $1/n = 1/1000$. The chance of having a streak of more than $3\lceil lgn \rceil = 30$ heads is $1/n^2 = 1/1000000$.

We now prove the complementary bound: the expected length of longest streaks of heads in $n$ coin flips is $\Omega(lgn)$.  

We divide the coin flips into interval with length of $s$, which means we are going to have $n/s$ interval. If we choose $s=\lfloor lgn/2\rfloor$ (5.4.3-1), we have to prove that it is likely that one of the groups comes up all heads, and hence it is likely the longest streaks has the length of $s = \lfloor lgn/2 \rfloor$.  
We partition into $n/ \lfloor lgn/2 \rfloor$ groups of $\lfloor lgn/2 \rfloor$ consecutive coin flips, and **we bound the probability that no groups are all heads** (5.4.3-2). Accoding to (5.8), we have:  
$Pr\{A_{i\lfloor lgn/2 \rfloor}\} = 1/2^{\lfloor lgn/2 \rfloor} \geq 1/\sqrt{n}$  
Which means, the probability that the streak starts at position $i$ does not exceed $\lfloor lgn/2 \rfloor$ is $1 - 1/\sqrt{n}$. Since the paritition groups are independent, the probability that no groups has streaks over $\lfloor lgn/2 \rfloor$ is:  
$(1 - 1/\sqrt{n})^{\lfloor {n/\lfloor lgn/2 \rfloor} \rfloor} \leq (1 - 1/\sqrt{n})^{n/\lfloor lgn/2 \rfloor - 1}$  

$\leq (1 - 1/\sqrt{n})^{2n/lgn - 1}$ (by $1 - \sqrt{n} < 1$)  

$\leq e^{-(2n/lgn - 1)/\sqrt{n}}$ (by $1 + x \leq e^x$)  

$= O(e^{-lgn})$ (by ${(2n/lgn - 1)/\sqrt{n}} \geq -lgn$ for sufficiently large $n$)  

We are going to prove that for sufficiently large $n$:  
$(2n/lgn - 1)/\sqrt{n} \geq lgn$  
$\Leftrightarrow 2n - lgn \geq \sqrt{n}{lg}^2n$  
$\Leftrightarrow (n - lgn) + \sqrt{n}(\sqrt{n} - {lg^2}n) \geq 0$ (5.10)  
We have $lgn = o(n)$, and:
$lim_{n\to +\infty} \frac{\sqrt{n}}{{{lg^2}n}} =^{L'Hopital}=lim_{n\to +\infty} \frac{{ln}^2}{4} \frac{n\sqrt{n}}{lnn}=+\infty$ since $lnn = o(n\sqrt{n})$.So (5.10) is proved.  

Back to the proof, the probability of the longest streak exceeds $\lfloor lgn/2 \rfloor$ is:  

$$\sum_{j=\lfloor lgn/2 \rfloor}^{n}Pr\{L_j\} = 1 - O(1/n)$$

Similar to the upper analysis, we can bound the expected longest streak:  
$E[L] = \sum_{i=0}^{n}Pr\{L_j\}$  

$= \sum_{j=0}^{\lfloor lgn/2 \rfloor - 1}j\cdot Pr\{L_j\} + \sum_{j=\lfloor lgn/2 \rfloor}^{n}j \cdot Pr\{L_j\}$  

$> \sum_{j=0}^{\lfloor lgn/2 \rfloor - 1}0\cdot Pr\{L_j\} + \sum_{j=\lfloor lgn/2 \rfloor}^{n} \lfloor lgn/2 \rfloor \cdot Pr\{L_j\}$ 

$= 0 \cdot \sum_{j=0}^{\lfloor lgn/2 \rfloor - 1}Pr\{L_j\} + \lfloor lgn/2 \rfloor\sum_{j=\lfloor lgn/2 \rfloor}^{n} \cdot Pr\{L_j\}$  

$= 0 + \lfloor lgn/2 \rfloor \cdot (1 - O(1/n))$  

$= \Omega(lgn)$ (5.11)

*Note:*
- 5.4.3-1: I have tried picking $s = \lfloor lgn \rfloor$ but it wasn't able to bound the equation to $O(g(x))$
- 5.4.3-2: the reason the aurthor chose to bound the probability of *no streaks* is to create an $O(g(x))$ bound for the concluding (5.11) equation. If reversedly we tried to bound the probability of at least 1 streaks, the we can only bound it to $\Omega(g(x))$, thus cannot bound the (5.11) equation since we cannot compare $\Omega(1/n)$ and $\lfloor lgn/2 \rfloor$. Not sure this is how the author thought tho.

***Solution 2***  
As with the brithday paradox, we can use indicator random variable to calcualte the approximate analysis. Define indicator random variable $X_{ik}$ is whether there is a streaks of at least length $k$ at position $i$. To calculate total number of such streaks:

$$X = \sum_{i=1}^{n - k + 1}X_{ik}$$

Taking expectations and linearity of expectation, we have:  
$E[X] = E[ \sum_{i=1}^{n - k + 1}X_{ik}]$  

$= \sum_{i=1}^{n - k + 1}E[X_{ik}]$  

$=  \sum_{i=1}^{n - k + 1}Pr\{X_{ik}\}$  

$=  \sum_{i=1}^{n - k + 1}1/2^k$  

$= \frac{n - k + 1}{2^k}$  

Pluggin $k$, we are able to calculate the number of streaks of length $k$. If this number is larger(much greater than 1), we are expect to see many streaks of length $k$ to occur and the probability that one occurs is high. If this number is small, we are expected to see few orrurence of streaks of length $k$ and the probability is kinda low. If $k = clgn$, for some positive constant $c$, we have:  
$E[X] = \frac{n - clgn + 1}{2 ^ {clgn + 1}}$  

$= \frac{n - clgn + 1}{n^c}$  

$= \frac{1}{n^{c - 1}} - \frac{(clgn + 1)/n}{n^{c-1}}$  

$= \Theta(1/n^{c-1})$    

If $c$ is large, then the expected number of streaks of length $k$ is really small and so is the probability. However, when $c$ is small, for exampl $c = 1/2$, then we obtain $E[X] = \Theta(n^{1/2})$, so we expect to see a large number of streak $k = 1/2lgn$. Therefore, one streaks is likely to occur.

For these rough estimates alone, we can conclude that the expected streaks to see is $\Theta(lgn)$.

