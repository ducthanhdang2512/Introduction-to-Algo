# 4.Divide and Conquer.  
## 4.1. The maximun-subarray problem.  
Given a array of n elements, find the maximun-subarray.  
**A brute force solution:**  
We can easily devise a brute-force solution by trying every possible subarray. This approach would take O(n^2) time.  
**A solution using divide-and-conquer.**  
Suppose we want to find a maximum subarray of the subarray A[low..high]. Divide-and-conquer suggests that we divide the subarraya into two subarrays of as equal size as possible.  
Therefore, we divide the subarray A[low..high] into 2 subarrays: A[low..mid], A[mid+1..high]. Let call the subarray with maximum sum is A[i..j].  
There are 3 options for the subarray belong to:  
+ Entirely in the subarray A[low...mid]. low <= i <= j <= mid.    
+ Entirely in the subarray A[mid+1..high]. mid+1<=i<=j<=high.    
+ Crossing the mid point. i <=mid < j.  

As we can see, the first two case, we can recall the result of A[low..mid] and A[mid+1..high].    
To solve the final case, we could devise 2 other maximum value of a subarray:  
+ maxL[i..j]: the maximum subarray of array A[i..j] that start from i-th position.  // save the position.
+ maxR[i..j]: the maximum subarray of array A[i..j] that end at j-th position.  // save the position.  
**Psuedocode:**
```
FindMaximum-Subarray(A,low,high):  
if low == high:  
	if a[low] < 0: maxL[low..high] = maxR[low..high] = max[low..high] = -1. (empty string will be a better choise).  
	maxL[low...high] = low.  
	maxR[low...high] = low.  
	max[low..high] = low.  
else:  
	(left_low,left_high,leftsum) = findMaximum-Subarray(A,low,mid)  
	(right_low,right_high,rightsum) = findMaximum-Subarray(A,mid+1,r)  
	(mid_low,mid_high,rightsum) = findMaximum-Subarray(A, maxR[low..mid],maxL[mid+1..r])  
	//
	maxL[low..high] = (max[mid+1..high] == -1 ? maxL[low..high] : max[mid+1..high]).  
	// same idea for maxR.  
	// compare three sum and return the opitmizing solution.  
```
**Note:** We also have a interesting method to solve this problem using binary-search... You can see the solution below for greedy approach:  
```
int maxSubArraySum(int a[], int size)   
{  
    int max_so_far = INT_MIN, max_ending_here = 0;  
    for (int i = 0; i < size; i++)  
    {  
        max_ending_here = max_ending_here + a[i];  
        if (max_so_far < max_ending_here)  
            max_so_far = max_ending_here;  
        if (max_ending_here < 0)  
            max_ending_here = 0;  
    }  
    return max_so_far;   
```  

## 4.2. Strassen algorithm for matrix multiplication.  
If you have seen matrices before, then you probably know how to multiply them.
If A = (a(ij)), B = (b(ij)) are square matrix N * N.  
We define the product C = A * B and the entry C(i,j).  
![Cij](Cij.png)  
We must compute n * n matrix entries, and each is the sum of n values.  
```
Square-Matrix-Multiply(A,B):  
n = A.rows
let C be a new n * n matrix.  
for i = 1 to n:  
	for j = 1 to n:  
		c(i,j) = 0;  
		for k = 1 to n  
			c(i,j) += a(i,k) * b(k,j)  
```
You might think that any matrix multiplication algorithm must take O(n^3) time, since the nature definition of matrix multiplication requires that many multiplications.  
You would be incorrect, in this section, we will se Strassen's remarkable recursive algorithm for multiplying n * n matrix. In runs in O(n^lg(7)) ~ O(n^2.81) time.  
**A simple divide-and-conquer algorithm**  
To keep things simple, when use a divide-and-conquer algorithm to compute the matrix product C = A * B, we assume that n is and extra power of 2 in each n * n matrices.  
We made assume this assumption because in each divide step, we will divide n * n matrices into for n/2 * n/2 matrixes.  
![MatmulDAQ](matmulDAQ.png)  
Each of these fore equations secifies two multiplications of n/2 * n/2 matrices and the addition of their n/2 * n/2 products. We can use these equations to create a straightforward, recursive, divide-and-conquer algorithm:
```
MatMultiply(A,B):  
n = A.rows  
let C be a new n * n matrix  
If n == 1:  
	c11 = a11 * b11;  
else:
	C11 = matMultiply(A11,B11) + matMultiply(A12,B21)
	C12 = matMultiply(A11,B12) + matMultiply(A21,B11)
	C21 = matMultiply(A21, B11) + matMultiply(A22, B12)
	C22 = matMultiply(A21,B12) + matMultiply(A22, B22)
	return C
```
Let T(n) be the time to multiply two n * n matrices using this procedure.  In the base case: T(1) = O(1)  
We can see in the Natmultiply psuedo code, we use recursively call matMultiply a total of eight times. Because each recursive call multiplies two n/2 * n/2 matrices, thereby contributing T(n/2) to the overall running time.  
Each of these matrices contains n^2/4 entries, and so each of the four matrix additions takes O(n^2) time.
--> T(N) = 8T(n/2) + O(N^2)  
This method still take O(N^3) to complete.
However, in Strassen Method, we only use 7 matMultiply instead of 8 like in the original method.
--> T(N) = 7T(N/2) + O(N^2).  
![strassen_formula](strassen_formula.png)
## 4.3. The subsititution method for solving recurrences.  
Now the we have seen how recurrences characterize the running times of divider-and-conquer algorithms, we will learn how to solve recurrences.  
We will start with "substitution" method, which comprises two steps:
+ Guess the form of the solution.  
+ Use mathematical induction to find the constants and show that the solution works.  

We can use this method to guess the upper/lower bounds on a recurrence.  
For example: T(N) = 2(T(n/2)) + n.  
+ Firstly, we guess the solution is T(n) = nlog(n).  
+ We start by assuming that this bound holds for all positive m < n, in particular for m = [n/2], yielding: T([n/2]) = c[n/2]log(n/2). Substituing into the recurrence yields:  
![mathinduction](mathinduction.png)  

**Making a good guess**  
Unfortunately, there is no general way to guess the correct solutions to recurrences. Guessing a solution takes experience and occassionally, creativity.  
As we can see more in the below example:  
T(n) = 2(T(n/2) + 17) + n.  
It could look difficult at first, when when have an extra "17" in the formula. However, you could realize that the different is not enough, so we could guess T(n) = O(nlog(n)).  
Another way to make a good guess is to prove loose upper and lower bounds on the recurrence and reduce the range of uncertainty.  

**Subtleties**    
Sometimes, you might correctly guess an asymptotic bound on the solution of a recurrence, but somehow the math fails to work out in the induction. The problem frequently turns out to be that in the inductive assumption is not strong enough to prove the detailed bound. If you revise the guess by subtracting a lower-order term when you hit such a snag, the math often goes through:  

Consider the recurrence.  
`T(n) =  2(T(n/2)) + 1.`  

We guess that the solution is T(N) = O(N) we try to show that T(n) <= C(n) for an appropriate choice of the constant c. Substituing our guess in the recurrence, we obtain.  
`T(n) <= 2c * T(n/2) + 1 = cn + 1.`  
Which do not prove `T(n) <= cn`.    

Instead of make a bigger guest, we could assume: T(n) <= cn - d. When d >= 0 is a constant. We now have:  
`T(n) <= 2 * (c(n) - d) + 1. = 2 * c(n) - 2*d +1 <= cn - d.`  
This statement is true as long as d >= 1.  
**Avoiding pitfalls**  
It is easy to err in the use of asymptotic notation. For example, we can falsely "prove: T(n) = O(N) by guessing T(n) <= c(n) and the arguing:  
![pitfalls](pitfalls.png)  
Since c is a constant, the error is that we have not proved the extract form of the induction hypothesis that is, `T(n) < c.n`, which lead us to wrong answers.  

**Changing variables**    
Sometime a little algebraic manipulation can make an unkown recurrence similar to one you have seen before. As an example, consider the recurrence:  
![changingvar1](changingvar1.png)    
Indeed, this new recurrence has the same solution: `S(m) = O(mlogm)`. Changing back from S(m) to t(n), we obtain:
`T(n) = T(2^m) = S(m) = O(mlogm) = log(lgn lg lgn).`    
![changingvar2](changingvar2.png)  
**4.4. The recursion-tree method for solving recurrences**  
As we can see in the substitution method, sometimes, you might have trouble to come up with a good guess. In this section, we examine `the recursion tree method` which typically better in devising a good guess.  
A recursion tree is best used to generate a **good guess**, which you can then verify by the subsitution method. When using a recursion tree to generate a good guess, you can often tolerate a small amount of **"sloppiness"**, since you will be verifying your guess later on.  
Let take a closer look in this example:  
![tree1](tree1.png)  
<-> T(n) = 3T(n/4) + cn^2  
![tree2](tree2.png)  
Now we add up the costs over all levels to determine the cost for the entire tree:  
![tree3](tree3.png)  
This last formula looks somewhat messy until we realize that we can again take advange of small amounts of sloppiness and use an infinite decreasing geometric series as an upper bound. Then we have:  
![tree4](tree4.png)  
Thus, we have derived a guess of T(n) = O(n^2) for our original recurrence T(n) = 3(T(n/4)) + O(n^2).  
Now we use the substitution method to verify that our guess was correct, that is, T(N) = O(n^2) is an upper bound for the recurrence T(n) = 3(T(n/4)) + O(n^2).  
We want to show that T(n) <= dn^2 for some constant d > 0. Using the same constant c > 0 as before, we have:  
![tree5](tree5.png)  
Where the last step holds as long as: d>= (16/13)c.  

In another, more intricate, example, Figure 4.6 shows the recursion tree for T(n) = T(n/3) + T(2n/3) + O(n).  
![tree6](tree6.png)  
We could guess our answer is T(n) = O(nlogn). Then again, use the **substituion method** to verify that O(nlogn) is an upper bound for the solution to the recurrence.  
We show that T(n) <= d nlogn where d is a suitable positive constant. We have:  
![tree7](tree7.png)  
## 4.5. The master method for solving recurrences.  
The master method provides a "cookbook" method for solving recurrences of the form:  
T(n) = aT(n/b) + f(n).
where a >= 1 and b > 1 are constants and f(n) is asymptotically positive function.  
**The master theorem**  
The master method depends on the following theorem:    
**Theorem 4.1(Master theorem)**  
![master theorem](mastertheorem.png)  
Before applying the master theorem to some examples, let's spend a moment trying to understand what it says. In each of the three cases, we compare the funtion f(n) with the function n^log(b, a).  
Intuitively, the larger of the two functions determines the solution to the recurrence.  
If, as in case 1, the funtion n^(log(b,a)) is larger, the the solution is T(n) = O(nlog(b,a)).  
If, as in case 2, the two functions are the same sizze, we multiply by a logarithmic faction, and the solution is T(N) = O(f(n)logn).  
If, as in case 3, the function f(n) is larger, then the solution is T(n) = O(f(n)).  
Must note that the three cases do not cover all the possibilities for f(n). There is a gap between cases 1 and 2, when f(n) is smaller than n^log(b,a) but not polynomially smaller. Similarly, there is a gap between cases 2 and 3 when f(n) is larger than n^log(b,a) but not polynomially larger. If the function f(n) falls into one of these gaps, or if the regularity condition in case 3 fails to hold, you cannot use the master method.  
**Using the master method**  
__*Example 1:*__  
T(n) = 9T(n/3) + n.  
For this recurrence. we have a = 3, b = 3 --> log(b,a) = 2, f(n) = n. And because n^log(b,a) = n^2 > n. We can apply case 1: T(n) =  O(n^2).  
__*Example 2:*__  
T(n) = T(2n/3) + 1.
We have a = 1, b = 3/2, f(n) = 1, log(b,a) = 0. Thus, n^log(b,a) = n, and therefore the solution to the recurrence is T(n) = O(lg(n)).  
__*Example 3:*__  
T(n) = 3T(n/4) = nlg(n).  
we have a = 3, b = 4, log(b,a) = 0.793.  
![example 3](masterex1.png)  
__*Example 4:*__  
![example 4](masterex2.png)  
__*Example 5:*__  	
T(n) = 2(T(n/2)) + O(n).
Characterizes the running times of the divide-and-conquer algorithm for both the maximum-subarray problem and merge sort. Here, we have a = b = 2, f(n) = O(n) thus we have n^(log(b,a)) = n. Applying case 2, we have T(n) = O(nlog(n)).  
__*Example 6:*__  
T(n) = 8(T(n/2))  + O(n^2).  
Describes the running time of the first divide-and-conquer algorithm that we saw for matrix multiplication. It falls to case 1, since log(b,a) = 3 > 2. Therefore, T(n) = O(n^3).  
__*Example 7:*__  
T(n) = 7(T(n/2)) + O(n^2).  
We have a = 7, b = 2. log(b,a) = 2.81 > 2. Case 1 applies, T(n) = n^2.81.  

## 4.6. Proof of the master theorem.  
### 4.6.1. The proof for exact powers.  
The first part of the proof of the master theorem analyzes the recurrence.  
T(n) = aT(n/b) + f(n).  
for the master method, under the assumption that n is an exact power of b > 1.  
We break the analysis into three lemmas:  
+ Lemma 1: reduces the problem of solving the master recurrence to the problem of evaluating an expression that contains a summation.  
+ Lemma 2 bounds on Lemma 1's summation.  
+ Lemm 3 puts the two together to prove a version of the master theorem for the case in which n is an exact power of b.

We will not go into detail in there, for more information, please refer to Introduction to Algorithm(Page 119 - 124).
**Lemma 4.1**  
![lemma1](lemma1.png)  
To prove the theorem, we could look at the recurrence tree.  
![l1tree](l1tree.png)  
In general, there are a^j nodes at depth j, and each has cost f(n/b^j).  
The cost of each leaf is T(1) = O(1), and each leaf is at depth log(b,n). since n = b^(log(b,n)) = 1. There are a^log(b,n) = n^(log(b,a)) leaves in the tree.  
We can obtain equations (4.21) by summing the costs of the nodes at each depth in the tre, as shown in the figure. The cost for all internal nodes at depth j is a^(j) * f(n/(b^(j))), and so the total cost is:  
![sumtree.png](sumtree.png).  
**Lemma 4.2**  
![lemma2](lemma2.png)  
**Lemma 4.3**  
![lemma3](lemma3.png)  
**Proof**  
![l3proof](l3proof.png)  












 


