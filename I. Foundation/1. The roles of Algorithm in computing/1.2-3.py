run = True
n = 1
while run:
    if 100 * (n**2) < 2**n:
        print(n)
        run = False
    n += 1