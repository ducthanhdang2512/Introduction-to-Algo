# LaTeX in .md file
To use LaTeX in .md file here, you have to enclose them:
* ```$...$``` : between 2 dollar sign to do inline math
* ```$$...$$``` : between 2 double dollar sign to do quote math (dont know how to explain it)

For example:
$f(n) = O(g(n))$ here I am using inline math, but if I want to emphasize or a big equation use the other:

$$g(n) = \Omega(g(n))$$

The basic symbol:

$\Theta$ : Capitalized Theta

$\theta$ : Uncapitalized Theta

$\Omega$ : Capitialized Omega

$\omega$ : Uncapitalized Omega

$$\sum_{n=1}^{\infty} 2^{-n} = 1$$	
\prod_{lower}^{upper}  
Limit $\lim_{x\to\infty} f(x)$ inside text	



