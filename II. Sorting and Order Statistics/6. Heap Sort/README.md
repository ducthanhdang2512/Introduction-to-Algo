# 6. Heapsort.  
In this chapter we will cover another sorting algorithm: Heap sort. There are few differences of heapsort when comparing to othere sortings algorithms:  
+ Like insertion sort, but unlike merge sort, heapsort sorts in place: only a constant number of array elements are stored outside the input array at any time.  
+ Heapsort also introduces another algorithm design technique: using a datastrure: "heap".  
## 6.1. Heap.  
The **(binary) heap** data structure is an array of object that we can view as a nearly complete **binary tree**.  
In this case, we can see heapsort data structure as a Interval Tree where:  
+ A node `i` have upmost 2 childs: `2*i` and `2*i+1`.  
+ In max-heap, `A[parent(i)] >= A[i]`. $\to$ The **biggest** element is at the root.  

![heap-tree](heap-tree.png)    
View a heap as a tree, we define the **height** of a node in a heap to be the number of edges on the longest simple downward path from the node to the leaf. We can see that its height is **$\Theta(lg(n))$**.  
The remainder of this chapter presents some basic procedures and shows how they are used in a sorting algorithm and a priority-queue data structure:  
+ The `MAX-HEAPIFY` procedure, which runs in $O(lg(n))$, is the key to maintaining the max-heap property.  
+ The `BUILD-MAX-HEAP` procedure, which runs in linear time, produces a max-heap from an unordered input array.  
+ The `HEADPSORT` procedure, which runs in $O(n lg(n))$ time, sorts an array in place.  
+ The `MAX-HEAP-INSERT`, `HEAP-EXTRACT-MAX`, `HEAP-INCREASE-KEY`, and `HEAP-MAXIMUM` procedures, which run in $O(lg(n))$ time, allow the heap data structure to implement in a priority queue.  

## 6.2 Maintaining the heap property.  
In order to maintain the max-heap property, we call the procedure MAX-HEAPIFY. Its inputs are an array A and an index i into the array. When it is called, MAX-HEAPIFY assumes that the binary trees rooted at LEFT(i) and RIGHT(i) are max-heaps, but that A[i] might be small that its children, thus violating the max-heap property.  
MAX-HEAP lets the value at A[i] "float down" in the max-heap so that the subtree rooted at index i obeys the max-heap property.  
```
MAX-HEAPIFY(A,i):
    l = 2*i // left child.
    r = 2*i + 1 // right child.
    if (l > A.heap_size) return // it means that i is a leaf.  
    else if (r > A.heap.size) max-child = l // child r does not exist. 
    else max-child = (a[l] > a[r] ? l : r)

    if (A[i] < A[max-child]):
        swap(A[i], A[max-child])
        MAX-HEAPIFY(A, max-child).
```  
![max-heapify](max-heapify.png)  
## 6.3. Building a heap.  
We can use procedure `MAX-HEAPIFY` in a bottom up manner to convert an array A[1..n], where n = A.length, into a max-heap. We can prove that elements in the subarray A[[n/2]+1...n] are all leaves of the tree, and so each is a 1-element heap to begin with. The procedure `BUILD-MAX-HEAP` goes through the remaining nodes of the tree and runs `MAX-HEAPIFY` ON each one. 
```
BUILD-MAX-HEAP(A):
    A.heap-size = A.length
    for i = [A.length/2] downto 1:
        MAX-HEAPIFY(A,i)
```
We can compute a simple upper bound on the running time of `BUILD-MAX-HEAP` as $O(nlog(n))$.  
However, we can derive a tighter bound by observing that the time for MAX-HEAPIFY to run at a node varies with the height of the node in the tree, and the heights of most node is small.  
We can focus on 3 details to derive our new bound for the algorithm:  
+ a n-heap tree has height $lg(n)$.  
+ At most $[n/2^{k+1}]$ nodes of any height h.  
+ the time required to by `MAX-HEAPIFY` when called on a node of height h is O(h).  

Therefore, the time required by `MAX-HEAPIFY` when called on a node of height h is O(h). So we can express the total cost of `BUILD-MAX-HEAP` as being bounded from above by:  
![build-max-heap](build-max-heap.png)  

$$S = \sum_{h=0}^{lg(n)} [n/2^{h+1}] * O(h) = O(n * \sum_{h=0}^{lg(n)} h/2^{h})$$  

We evaluate the last summation by substituing x = 1/2 in the formula (A.8) yielding  

$$\sum_{k=0}^{\infty} kx^{k} = \frac{x}{(1-x)^2} (A.8)$$  

$$Thus:\sum_{h=0}^{\infty} \frac{h}{2^h} = \frac{1/2}{(1 - 1/2)^2}=2$$  

Therefore, we can bound the running time of `BUILD-MAX-HEAP` as:

$$S = \sum_{h=0}^{lg(n)} [n/2^{h+1}] * O(h) = O(n * \sum_{h=0}^{lg(n)} h/2^{h}) = O(n)$$  

## 6.4. The heapsort algorithm.  
The heapsort algorithm starts by using `BUILD-MAX-HEAP` to build a max-heap on the input array A[1..n] where n = A.length.  
Since the maximum element of the array is stored at the root A[1], we can put it into its correct final position by exchanging it with A[n] and discard node n from the heap by decreasing heap_size.  
After that, to maintain the `max-heap-property`, all we need is to perform `MAX-HEAPIFY(A,1)`.  
```
HEAPSORT(A):
    for i = A.length downto 2:
    exchange A[1] with A[i].  
    A.heap_size--
    MAXHEAPIFY(A,1)
```  
The `HEAPSORT` procedure takes time $O(nlg(n))$  
**Excercise 1:** Describe heapsort using figure 6.4.  
![heap-sort](heapsort.png)  
In the picture (b), we swap A[1] = 16 and A[n] = 1, then eliminate A[n] from the heap-tree. Now, the A[1] = 1 is not the maximum value of the heap-tree, thus we need to use `MAXHEAPIFY(A,1)` to take the A[1] to the leaf.  
From the picture (c) to (j), we gradually discard A[heap-size] from the tree until nothing lefts.  
After that, we have picture (k), which satisfies our question.  
## 6.5. Priority queues.  
A **priority queue** is a data structure for maintaining a set S of elements each with an associated value called as **key**. A **max-priority queue** supports the following operations.  
+ INSERT(S,x): insert the element x into the set S.
+ MAXIMUM(S): return the element with the largest key.  
+ EXTRACT-MAX(S): removes and returns the element of S with the largest key.  
+ INCREASE-KEY(S,x,k): increases the value of element x's key to the new value k.  

**MAXIMUM(S):**  
```
HEAP-MAXIMUM(A):
    return A[1]
```
**HEAP-EXTRACT-MAX(A):**  
It works similar to `HEAP-SORT` property, where we need to discard the maximum value from the tree.  
```
HEAP-EXTRACT-MAX(A):
    IF A.heap-size <1:
        return "heap underflow".
    max = A[1];
    A[1] = A[heap-size];
    heap-size--
    MAX-HEAPIFT(A,1)
```  
**Increase a key:**  
```
HEAP-INCREASE-KEY(A,i,key):
    if (key < A[i]):
        return error: "new key is smaller than current key"
    A[i] = key
    while i > 1 and A[parent(i)] < A[i]:
        swap(A[parent(i), A[i]])
        i = parent(i)
```
**Max-heap-insertion:**
```
MAX-HEAP-INSERT(a,key):
    heap-size++
    A[heap-size] = -INF.
    HEAP-INCREASE-KEY(A, heap-size, key)
```  
**Excercise: Young tableus**  
An m x n **Young tableau** is an m x n matrix such tha the entries of each row are sorted order from left to right and the entries of each column are sorted ordered from to the bottom. Some of the entries of a Young tableau may be $$\inf**, which we treat as non-existent elements.  
**a.** Draw a 4 x 4 Young tableau contain the elements {9 16 3 2 4 8 5 14 12}.  
|   |   |   |   |
|:-:|:-:|:-:|:-:| 
| 2 | 4 | 5 | 9 |  
| 3 | 8 | 14| 16|  
| 12| 0 | 0 | 0 |  
| 0 | 0 | 0 | 0 |  
**NOTE:** 0s represents empty entries.  
**b.** EXTRACT-MIN.
```
EXTRACT-MIN(A):
    minval = A[1][1]
    A[1][1] = INF
    x = 1, y = 1 
    while x <=m and y <= n:
        left = A[x+1][y]
        right = A[x][y+1]
        if left == right == INF: break
        if left <= right: 
            swap(A[x][y], A[x+1][y])
            x++
        else: 
            swap(A[x][y], A[x][y+1])
            y++
    return A[1][1]
```  
**c.** Show how to insert a new element into a nonfull m x n Youngtableau in O(m+n) time.  
First we set A[m][n] = key. Then we in each "climb" action we have 2 options:  
+ climb to the left-adjacent entry.
+ climb to the up-adjacent entry.
In this situation, prefer to adjacent entry which has bigger key.  
```
INSERT(key):
    x = m
    y = n
    A[m][n] = key
    #define prevL a[x][y-1]
    #define preU a[x-1][y]
    #define cur a[x][y]
    while preL > key || preR > key:
        if (preL > key):
            swap(preR, cur)
        else: if (preR > key):
            swap(preL, cur)
            else {
                if preL > preU: swap(preL, cur)
                else swap(preR, cur)
            }
```
**d.** Using no other sorting method as a subroutine, show how to use an n x n Young tableau to sort n^2 number in O(n^3).  
Our algorithm is described as follows:
- Insert all n^2 number into Young tableau. This action takes $O(n^3)$ time.  
- using `EXTRACT-MIN` to extract the smallest number. Do it for $O(n^3)$ time.  

        


















