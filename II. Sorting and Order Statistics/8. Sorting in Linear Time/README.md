# 8. Sorting in Linear Time

### Prologue
Previously we have examined about several algorithms. They have a major similiarity: based on comparision between two input elements. As such, we call them the *comparison sorts*. In section 8.1, we shall prove that **all comparison sorts** must make $\Omega(nlog)$ comparisons in the worst case scenario to sort $n$ elements. In the following sections, we shall examine other sorting algorithms which run in *linear time* (which sounds much better right?)

## 8.1 Lower bound for sorting
Suppose we have an input sequence $(a_1, a_2, ..., a_n)$  
In comparison sorts, we use only the comparison to gain information about the input sequence. We compare the elements $a_i$ with $a_j$ to gain the knowledge about their relative order. We do not gain information about their values or other information.  
Without the loss of generality, we suppose all values are distinct, and all comparison are in the form of $a_i \leq a_j$.  
### **The decision tree-model**
A *decision tree-model* are a binary tree represents all the comparions making while using a comparison algorithms. All other aspects of the algorithm such as data movement, etc. are ignored. Below are a decision tree of the input sequence of 3 elements:

![Decision tree](decision-tree.PNG)

In a deision tree, we mark each node with a $i:j$ form with $1 \leq i \leq j \leq n$ which means we are comparing $a_i$ and $a_j$. The left subtree means that $a_i \leq a_j$, the right tree means that $a_i > a_j$.  We also annotate each leaf as a permutation $(\pi(1), \pi(2), ..., \pi(n))$. When we come to the leaf, it means that the order is established as $a_{\pi(1)} \leq a_{\pi(2)} \leq ... \leq a_{\pi(n)}$.The order of comparisons in the algorithm can be determined by tracing the path from root to the leaf. Each of the $n!$ permuation must appear as one of the leaves for a comparison to be correct. Furthermore, this leaves must be "reachabe" from the root as an actual execution of the algorithm

### **A lower bound for worst case**
***Theorem 8.1***  
*Any comparison sort requires $\Omega(nlogn)$ comparisons in worst-case.*  

Let $x$ be the maximum number of comparisons needed to be made in the sorting algorithm. Then the path from the root to the worst-case leaf must be of length $x$, which means that the maximum height of the tree is $x$. As a result, it has at most $2^x$ leaves.  
On the other hand, each of the $n!$ permutation must appear as one of the leaves. Therefore:  
$n! <= 2^x$  
$log_2(n!) <= x$  

Since $log_2(n!) = \Theta(nlogn)$, we have:

$$x = \Omega(nlogn)$$

***Corollary 8.2***  
*Heap sort and Merge sort are 2 optimal asymtotically comparison sort.*  
Heap sort and Merge sort running times is $O(nlogn)$ mathches the $\Omega(nlogn)$ running times in *Theorem 8.1*.

*Pretty good further reading about another proof: https://www.cs.cmu.edu/~avrim/451f11/lectures/lect0913.pdf*

## 8.2 Counting sort
***Counting sort*** assumes that each of the *n* input elements is an integer range from $0$ to $k$. If $k = O(n)$, the running time is $\Theta(n)$. Counting sort determines, for each elemement $x$, how many elements are lower than $x$, therefore determines its position in the output sequences. For example, if $x$ is greater than $17$ elements, then it is going to stay at position $18$.  
Here is the pseudo-code:
```
COUNTING-SORT(A,B,k):
    let C[0..k] be new array
    for i = 0 to k:
        C[i] = 0
    for j = 1 to A.length:
        C[A[j]] = C[A[j]] + 1
    // now contains number of elements has value = i
    for i = 1 to k:
        C[i] = C[i-1] + C[i]     
    // now contains the number of elements with value <= i
    for j = A.length downto 1: // for stable property
        B[C[A[j]] = A[j]
        C[A[j]] = C[A[j]] - 1 
        // because of this, if j: 1 to A.length the the stable is not reserved 
```   
*Explaination*  
B is the output sequence. C acts like a counting table for each value. In the last **for** loop, we decrease `C[A[j]]` in case the input values are not distinct. The last loop goes from `A.length` down to `1` to keep the ***stable property***, which will be illustrated later.  
*Running time*  
The first loop and the third loop both have $\Theta(k)$ running times. The second and the fourth loop both have $\Theta(n)$ running time. Total, the counting sort running time is $\Theta(n + k)$. In practice, with $k = O(n)$, we have the running time $\Theta(n)$. Since counting sort does **not** use any comparison, the running time beats the $\Omega(nlogn)$ of the comparison sorts.  
***Stable*** *property*  
The stable property of counting sort is that: *which number appears first in the input array will appear first in the ourput array. For example, with the input sequence $A(2,5,3,2,1)$, the first number $2$ (`A[0]`) will appear before the second number $2$ (`A[4]`) - in the same order  as in the input sequence - in the output sequence.  
This property is important in carrying satellite data, and also in the following ***radix sort***.

## 8.3 Radix sort
*Introduction*  
Consider this: you have three number $75,57,73$, how would you compare them? Instinctively, you will compare from right to left. For example, $75$ and $73$, you compare the right-most digit $7$. Since they are equal, you compare the next one $5$ and $3$. Since $5 > 3$ you get $75 > 73$.  
Radix sort is reverse: you compare every digits from the left to the right. In another word, sort based on the *least significant bit* till the *most significant bit*.  

![Radix sort example](radix-sort.PNG)

*Code*  
The code is pretty straight-forward. Suppose we have an input sequence A whose elements each has *d*-digits:
```
for i = 1 to d:
    use a stable sort to sort array A on digit i
```  
*Insight*  
It is not necessarily the digit. Each digit is just one `'key'` that the radix sort uses to sort. We can sort using  multiple fields as keys such as days, months, years.  

***Lemma 8.3***  
Given $n$ *d*-digit numbers in which each digit can take up on $k$ possible values, the RADIX-SORT correctly sorts these numbers in $\Theta(d(n+k))$ if the stable sort it uses takes $\Theta(n+k)$.  

***Proof*** At each step, we sort based on each of the digits of $n$ elements. Since the digits can have $k$ possible values, each step takes $\Theta(n+k)$ (If the digits range from $0$ to $k$, *counting sort* is a good choice). $d$-digts comes with $d$ steps, so the running times is $\Theta(d(n+k))$.  
When $d$ is constant and $k = O(n)$, radix sort runs in linear time.

At each step, key can be a group of digits instead of one. 
***Lemma 8.4***  
Given $n$ *b*-bit numbers and a positive number $r \leq b$, RADIX-SORT correctly sorts these numbers in $\Theta((b/r)(n + 2^r))$ time if the stable sort it uses takes $\Theta(n + k)$ times for inputs in range from 0 to k.

***Proof*** For a value of $r \leq b$, we view each key having the length of $\lceil b/r \rceil$ digits of $r$ bits. Each digits ranges from $0$ to $2^r - 1$. So each pass of counting sort takes $\Theta(n + 2^r)$. There are $d$ passes total, so the running time is $\Theta((b/r)(n + 2^r))$.  

For given values of $n$ and $b$, we wishes to choose the value of $r$ to minimize the expression $(b/r)(n+2^r)$.  
* If $b < \lfloor lgn \rfloor$:  
For any value $r$ we have $\Theta(n+2^r) = \Theta(n)$. If we choose $r = b$, the running time is linear: $\Theta((b/b)(n + 2^b)) = \Theta(n)$. 
* If $b \geq \lfloor lgn \rfloor$:  
Choosing $r = b$ yields the running time of $\Theta(bn/lgn)$. This is the best choice, due to the the following.  
Since $2^r$ increases faster than $1 / r$ decreses, choosing any $r > \lfloor lgn \rfloor$ will result in running time of $\Omega(bn/lgn)$.  
If we choose $r < \lfloor lgn \rfloor$, the fraction $b/r$ increases while $n + 2^r$ remains at $\Theta(n)$.  

*Reality*  
Let's compare radix sort to other comparison-based sort, this case quick sort.  
If $b = O(lgn)$, which is often the case, and we choose $r \approx lgn$, the running time is $\Theta(n)$ - asymtrically optimal compared to $\Omega(nlgn)$, and the expected running time of $\Theta(nlgn)$ of quick sort. However, the constant factors is hidden in the $\Theta(n)$ expression. Although radix sort takes less passes compared to quick sort, each pass is significantly longer.  
In addition, quick sort often uses hardware caches more effectively than radix sort, plus that quick sort is an in-place sort - which does not require external memory, if memory is at a premium, quick sort is preferrable.  

## 8.4 Bucket sort
***Bucket sort*** assumes that the input is drawn from a uniform distribution and has average running-time of $O(n)$. Like counting sort, bucket sort assumes something about the input: they are generated by a random process which uniformly distributes elements and has value in range $[0,1)$.  
*Concept*  
The bucket sort divides the interval $[0,1)$ into $n$ equal-sized subintervals called **buckets**. Since inputs are uniformly distributed over the interval, we do not expect many to go into the same numbers.  
*Code*  
```
let B[0..n-1] be new array
n = A.length
for i = 0 to n - 1:
    make B[i] empty list
for i = 1 to n:
    bucket_id = floor(nA[i])
    insert A[i] into the list B[bucket_id]
for i = 0 to n - 1:
    sort the list B[i] with insertion sort
concatenate the list B[0], B[1], B[2], ..., B[n-1] together in order. 
```  
B is a linked list in which each element connect to the next element.  

*Running time*  
The first and the third loop runs in $O(n)$ time. So the only running time we need to consider is the second loop (which is very math-heavy, you do not necessarily read it)  
Let $T(n)$ be the running time of the second loop, random variable $n_i$ be the number of elements placed in bucket $i$. Since the insertion sort runs in quadratic time, then we have:  

$$T(n) =  \Theta(n) + \sum_{i=0}^{n-1}O({n_i}^2)$$ 

Taking expectiation at both sides:  

$E[T(n)] = E\Big[\Theta(n) + \sum_{i=0}^{n-1}O({n_i}^2)\Big]$  

$E[T(n)] = \Theta(n) + E\Big[\sum_{i=0}^{n-1}O({n_i}^2) \Big]$ (By linearity of expectation)  

$E[T(n)] = \Theta(n) + \sum_{i=0}^{n-1}O(E[{n_i}^2])$ (By equation C.22) (8.1)  

Define the indicator random variable:  

$$X_{ij} = I\{\text{A[j] falls in bucket i}\}$$

Therefore:   

$$n_i = \sum_{j=1}^{n}X_{ij}$$  

Now we compute $E[{n_i}^2]$ by expanding the square equation:  

$E[{n_i}^2] = E\Big[\Big(\sum_{j=1}^{n}X{ij}\Big)^2\Big]$   
$=E\Big[\sum_{j=1}^{n}\sum_{k=1}^{n}X_{ij}X_{ik}\Big]$  

$=E\Big[\sum_{j=1}^{n}{X_{ij}}^2 + \sum_{j=1}^{n}\sum_{k=1;k \neq j}^{n}X_{ij}X_{ik}\Big]$  

$=E\Big[\sum_{j=1}^{n}{X_{ij}}^2] + E\Big[\sum_{j=1}^{n}\sum_{k=1;k \neq j}^{n}X_{ij}X_{ik}\Big]$ (By linearity of expectations) (8.3)  
  
Indicator $X_{ij}$ has value 1 of probability $1/n$ and has value $0$ otherwise. Therefore:  

$E[X_{ij}^2] = 1^2 \cdot \frac{1}{n} + 0^2 \cdot \Big(1 - \frac{1}{n}\Big)$
$=\frac{1}{n}$

When $k \neq j$, $X_{ij}$ and $X_{ik}$ are actually independent. Therefore:  
$E[X_{ij}X_{ik}] = E[X_{ij}]E[X_{ik}]$  

$=\frac{1}{n} \cdot \frac{1}{n}$  

$=\frac{1}{n^2}$  

As a result, equation (8.3) equals:  
$E[{n_i}^2] = \sum_{j=1}^{n} 1/n + \sum_{j=1}^{n}\sum_{k=1;k \neq j}^{n} 1/n^2$  
$=1 + 1 - n \cdot 1/n^2$  
$=2 - 1/n$  

Replace in equation (8.1), we have:  
    
$$E[T(n)] = \Theta(n) + n \cdot O(2 - 1/n) =\Theta(n)$$

*Other*  
Even if the input may not be evenly distributed, bucket sort may still run in linear time. As long as the sum of squares of bucket size is linear in the total number of elements, equation (8.1) tells us that bucket sort runs in linear time. (*Honestly I don't understand this part. If you understand contact me please!*)