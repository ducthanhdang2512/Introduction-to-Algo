# 7. Quicksort
The quicksort algorithm has a worstcase running time of $\Theta(n^2)$ on an input array of n numbers. Despite this slow worst-case running time, quicksort is often the best practical choice for sorting because it is remarkably on avaerage with expected running is $\Theta(nlg(n))$  
## 7.1. Description of quicksort.  
Quicksort, like merge sort, applies the divide-and-coquer paradigm. Here is the three-step divide-and-conquer process for sorting typical subarray A[p..r]:  
**Divide:** Partition (rearrange) the array A[p..r] into two subarrays (possibly empty) A[p..q-1] and A[q+1..r] such that each element in A[p..q-1] $\leq$ A[q] and each element in A[q+1..r] $\geq$ A[q].  
**Conquer:** Sort the two subarrays A[p..q-1] and A[q+1...r] by recursive call to quicksort.  
**Combine:** Because two subarrays are already sorted, no work is needed to combine them.  
```
QUICKSORT(A, p, r)
if p < r:
    q = PARTITION(A, p, r)
    QUICKSORT(A, p, q-1)
    QUICKSORT(A, q+1, r)
```  
**Partitioning the array**  
The key to the algorithm is the **PARTITION procedure**, which rearranges the subarray A[p..r] in place.
```
PARTITION(A, p, r):
    x = A[r]
    i = p - 1
    for j = p to r - 1
        if A[j] <= x:
            i++
            swap(A[i], A[j])
swap(A[i+1], A[r])
return i+1
```  
Figure 7.1 shows how PARTITION works on an 8-element array.  
![partion](partion.png)  
## 7.2. Performance of quicksort.  
The running time of quicksort depends on whether the partitioning is balanced or unbalanced, which in turn depends on which elements are used for partitioning. If the partitioning is balanced, the algorithm will runs asymptotically as fast as merge sort, otherwise, It will runs asymptotically as slow as insertion sort.  
**Worst-case partitioning**  
In the worst case behavior for quicksort occurs when the partitioning routine produces one subproblem with n-1 elements and one with 0 elements.  
T(n) = T(n-1) + T(0) + $\Theta(n)$ = T(n-1) + $\Theta(n)$ = $\Theta(n^2)$  
**Best-case partitioning**  
In the most even possible split, PARTITION produces two subproblems, each of size no more than n/2. In that case, we have much better algorithm.  
T(n) = 2T(n/2) + O(n).  
Use **Master theorem** or draw a **recursion tree**, we have:  
T(n) = n lg(n).  
**Balanced partitioning**  
The average-case running time of quicksort is much closer to the best case than the worst case, as the analyses in Section 7.4 will show. They key is understanding why is to understand how the **balance of partitioning**  is reflected in the recurrence that describes the running time.  

![balancedPartition](balancePartition.png)  
Suppose, for example, that the partitioning algorithm always produces a 9-to-1 proportional split, which at first blush seems quite unbalanced. We the obtain the recurrence.  
T(n) = T(9/10n) + T(n/10) + cn.  
on the running time of quicksort. Notice that every level of the tree has cost cn, until the recursion reaches a boundary condition at depth $\log_{10} n$ and then the levels have cost at most cn.  
The recursion terminates at depth $\log_{10/9} n = \Theta(lg n)$  
--> The total cost of running time is $\Theta(nlg(n))$.  
In fact, any split of constant proportionality yields a recursion tree of depth $\Theta(lg n)$, where the cost at each level is O(n).   
**Intuition for average case**  
![intuition](intuition.png)  
To develop a clear notion of the randomized behavior of quicksort, we must take an assumption about how frequently we expect to encounter the various inputs.  As in our probabilistic analysic of the ehiring problem, we will assume for now that all permutations of the input numbers are equally likely.  
In average case, PARTITION produces a a mix of **"good" and "bad" splits**, which shows in the figure 7.5 above.  
We consider this situation, where at the root of the tree, the cost is n for partitioning, and the subarrays produced have sizes n-1 and 0 - the worst case.  
At the next level, the subarray of size n-1 undergoes best-case partitioning into subarrays of size (n-1)/2 - 1 and (n-1)/2. Let's assume the boundary-condition cost is 1 for the subarray of size 0.  
The combinations if the bad split followed by the good split produces three subarrays of sizes 0, (n-1)/2 - 1 and (n-1)/2 at a combined partitioning cost of $\Theta(n) + \Theta(n-1)$. Certainly, this situation is no worse than that in Figure 7.5(b), namely a single level of partitioning that produces two subarrays of size (n-1)/2 at a cost $\Theta(n)$.  
Intuitively, **the $\Theta(n-1)$ cost of the bad split can be absorbed into the $\Theta(n)$ cost of the good split** and the resulting split is good.  
Thus, the running of quicksort still is O(nlogn), but with a slightly larger constant hidden by the O-notation.  
**Excercises**  

**_7.2-5:_** Suppose that the splits at every level of quicksort are in the proportion 1-a and a, where $0 < a \leq 1/2$ is a constant. Show that the minimum depth of a leaf in the recursion tree is approximately -$\lg n/\lg a$ and the maximum depth is approximately $-\lg n/ \lg(1-a)$.  
**Solution:**  
We have recurrence:  
T(n) = T(an) + T((1-a)n) + cn.  
Refer to **Balanced partitioning** in section 7.2. We can see the depth of a recursion tree with T(n) = aT(n)/b is $\log_{b} an$
For example: T(n) = 2T(n/2) then the depth of the recursion tree is: $\log_{2} 2n = \log(n)$.  
Therefore:
Minimum depth of a leaf in the recursion tree is: $\lg_{1/a} n = -\lg_{a} n = -\lg n / \lg a$.  
Maximum depth of a leaf in the recursion tree is: $\lg_{1/(1-a)} n = -\lg_{1-a} n = -\lg n / \lg (1-a).$  

**_7.2-6:_** Argue that for any constant $0 < a \leq 1/2$, the probability is approximately 1 - 2a that on a random input array, PARTITION produces a split more balanced than 1-a to a.  
**Solution:**  
Let k denote the number of entries of A which are less than A[n] in a PARTITION.  
In order to produce a split more balanced than: 1-a to a, then:  
$an \leq k \leq (1-a)n$.
$\rightarrow P(k) = (1-a)n - an + 1 / n \approx (1-2a).$  
## 7.3. A randomized version of quicksort.  
In exploring the average-case behavior of quicksort, we have made an assumption that all permutations of the input numbers are equally likely. In an Engineering situation, however, we cannot always expect this assumption to hold.  
As we saw in Section 5.3, we can sometimes add randomization to an algorithm in order to obtain good expected performance over all inputs.  
In quicksort, we use a randomization technique called **random sampling**, yields a simpler analysis.  
Instead of always using A[r] as the pivot, we will select a randomly chosen element from the subarray A[p..r].  
By randomly sampling the range p...r, we ensure that the pivot element x = A[r] is equally likely to be any of the r - p + 1 elements in the subarray. Because we randomly choose the pivot element, we expect the split of the input array to be reasonably well balanced on average.  
The changes to PARTITION and QUICKSORT are small. In the new partition procedure, we simply implement the swap before actually partitioning:  
```
RANDOMIZE-PARTITION(A,p,r)
    i = RANDOM(p,r)
    swap(A[r], A[i])
    return PARTITION(A,p,r)
RANDOMIZED-QUICKSORT(A,p,r):
    if (p < r):
        q = RANDOMIZE-PARTITION(A,p,r)
        RANDOMIZED-QUICKSORT(A,p,q-1)
        RANDOMIZED-QUICKSORT(A,q+1,r)
```  
## 7.4. Analysis of quicksort.  
### 7.4.1. Worst-case analysis.  
We saw in Section 7.2 that a worst-case split at every level of recursion in quicksort produces a $\Theta(n^2)$ running time, which intuitively, is the worst-case running time of the algorithm.  
### 7.4.2. Expected running time.  
We have already seen the intuition behind why the expected running time of RANDOMIZED-QUICKSORT is O(n lg n).  
In this part, we can analyze the expected running time of RANDOMIZED-QUICKSORT precisely by first understanding how the partitioning procedure operates and the using this understanding to derive an O(n lg n) bound on the expected running time.  
We assume throughout that the values of the elements being sorted are distinct.  
**Running time and comparision**  
The **QUICKSORT** and **RANDOMIZED-QUICKSORT** procedures differ only in how they select **pivot elements**. We can therefore couch our analysis of RANDOMIZED-QUICKSORT by discussing the QUICKSORT and PARTITION procedures, but with the assumption that pivots elements are selected randomly from the subarray passed to RANDOMIZED-PARTITION.  
```
PARTITION(A, p, r):
1   x = A[r]
2   i = p - 1
3   for j = p to r - 1
4       if A[j] <= x:
5           i++
6           swap(A[i], A[j])
7   swap(A[i+1], A[r])
return i+1
```  
**Lemma 1:** Let X is the number of comparisions performed in line 4 of PARTITION over the entire execution of QUICKSORT on an n-element arrays. Then the running time of QUICKSORT is O(n + X).  
In QUICKSORT, we call at most n times PARTITION procedures, and the total running time of PARTITION over the entire execution of QUICKSORT is appropriately X.  
To calculate X, we must understand when will compare two elements of the array first.  
Let rename the elements of array as: ${z_{1},z_{2},z_{3},..,z_{n}}$ where $z_{i}$ being the i-th smallest number.  
Define $X_{ij} = I(z_{i} is compared to z_{j})$  
where I(f(x)) = 1 when statement f(x) is true and otherwise.  
Therefore: $X = \sum_{i = 1}^{n-1} \sum_{j=i+1}^{n} X_{ij}$  

Taking expectations of both sides, and the using linearity of expectation and Lemma 5.1, we obtain:  

$$E[X] = E[\sum_{i = 1}^{n-1} \sum_{j=i+1}^{n} X_{ij}]$$  

$$= \sum_{i = 1}^{n-1} \sum_{j=i+1}^{n} X_{ij} E[X_{ij}]$$  

$=\sum_{i = 1}^{n-1} \sum_{j=i+1}^{n} X_{ij}$ Pr($z_{i}$ is compared to $z_{j}$)  
It remains to compute the Pr($z_{i}$ is compared to $z_{j}$). Our analysis assumes that RANDOMIZED-PARTITION procedures choose each pivot randomly and independently.  
Let us think about two items are not compared. Consider and input to quicksort of the numbers 1 through 10 (in any order) and support that the first pivot is 7. Then the first call to PARTITION seperates the numbers into two sets: {1,2,3,4,5,6} and {8,9}. In doing so, the pivot 7 is compared to all other elements, but no number from first set (e.g. 2) is compared to another number from the second set (e.g. 9).  
  
In general, because we assume that the element values are distinct, once a pivot x is chosen with $z_{i}$ < x < $z_{j}$, we know that $z_{i}$ and $z_{j}$ cannot be compared to each other.  
In other hand, if $z_{i}$ or $z_{j}$ is chosen as a pivot before any other items in subarray ${z_{i}, z_{i+1}, ... ,z_{j}}$ then $z_{i}$ and $z_{j}$ will be compared to each other.  
Therefore, we have:  
![randomizedProb](randomizedProb.png)  
Therefore:  
$E[X] = \sum_{i = 1}^{n-1} \sum_{j=i+1}^{n} \frac{2}{j-i+1}$  
We can evaluate this sum using a change of variables k = j - i  and the bound on the harmonic series in equation (A.7):  
![proveProb](proveProb.png)  

$\sum_{k=1}^{n} \frac{1}{n} <= lg(n) (A.7)$  

**Note:** Another way to improve QUICKSORT algorithm is to partition around a pivot that is chosen more carefully than by picking a random element from a subarray. One common approach is the **median-of-3** method: choose the pivot as the median of a set of 3 elements randomly selected.  
 

  




























