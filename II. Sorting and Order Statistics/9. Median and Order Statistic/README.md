# 9. Medians and Order Statistics.  
The i-th **order statistic** of a set of n elements is the *i-th* smallest elements.  
A **median**, informally, is the "halfway point" of a set.  
This chapter addresses the problem of selecting the *i-th* order statistic from a set of n **distinct** numbers.
__**Problem:**__  
**Input:** A set A of n distinct numbers and an integer i, with 1 <= i <= n.  
**Output:** the element $x \in A$ that is larger than exactly i-1 other elements of A.  
We can solve this problem in $O(nlog(n))$ time by using sorting algorithm.  
However, in this chapter we would like to present a faster algorithm.  
- In section 9.1, we examine the problem of selecting the minimum and maximum of a set of n elements.  
- Section 9.2 analyzes a practical randomizezd algorithm that achieves an O(n) expected running time.  
- Section 9.3 contains an algorithm of more theoretical interest that achieves the O(n) running time algorithm.  
## 9.1 Minimum and Maximum.  
**The basic method:**  
```
MINIMUM(A):
min = A[1].
for i = 2 to A.length:
    if min > A[i]
        min = A[i].
return A.
```
It takes n-1 comparisions to find the mimimum value, but can we do better?  
Think of any algorithm that determines the minimum as a tournament among the elements. Each comparision is a match in the tournament in which the smaller number of the two elements wins.  
Observing that every element except the winner must lost at least one match, we can concludes that n-1 comparisions are necessary to determine the minimum.  
**Simultaneous minimum and maximum**  
In some applications, we must find both the minimum and the maximum of a set of n elements.  
Use the **basic method**, we can find both the minimum and the maximum for a total of 2n-2 comparisions.  
In fact, we can optimize our algorithm by comparing [n/2] pairs of elements in the set. It costs 3n/2 comparisions.  
```
MIN-MAX(A):
    MIN =  INF
    MAX = -INF
    for(i = 1;i+1<=n;i+=2){
        if (A[i] > A[i+1]) swap(A[i], A[i+1])
        MIN = min(A[i], MIN)
        MAX = max(A[i], MAX)     
    }
    if (i == n)
        MIN = min(A[n], MIN)
        MAX = max(A[n], MAX)
```  
**Exercise:**
**Ex1:** Show that the second smallest of n elements can be found with n + [lgn] - 2 comparisions in the worst case.  
**Solution 1:**  
    We wil be recursing by diving the array into two equal size sets of elements, we will neglect taking floors and ceilings.  
    Break up the elements into disjoint pairs. Then compare each pair. Consider only the smallest elements from each. From amongh this set of elements, the result will be either what had been paired with the smallest elements, or what what is the second smallest elements of the sub-problem.  
    Therefore, we have: T(n) = T(n/2) + n/2 + 1.  
    T(2) = 1.
    Assume that T(n) <= n + [lg(n)] - 2, which is true for the T[2].  
    $T(n) = T(n/2) + n/2 + 1 \leq n/2 + lg(n/2) - 2 + n/2 + 1$   
    $T(n) \leq n + lg(n) - 1 - 2 + 1 = n + lg(n) - 2$  
**Solution 2:**  
    Build the BST (Binary Search Tree) from bottom to top, each node contains the smaller one of a pair, we need n-1 comparision to find the minimum and traces the 2-th mimimun relies on the fact that the second smallest number only be "defeated" by the smallest number.  
    Because of the height of the BST is lg[n].  
    The total comparisions is n-1 + lg[n] - 1  = n + lg[n] - 2.  

## 9.2. Selection in expected linear time    
In this chapter, we present an $\omega (n)$ algorithm using divide-and-conquer algorithm for selection problem.  
The algorithm **RANDOMIZED-SELECT** is modeled after the quicksort algorithm of Chapter 7. As in quicksort, we partition the input array reccursively. But unlike quicksort, which recursively processes both sides of the partition, **RANDOMIZED-SELECT** works on only one side of the partition.  
This difference show up in the analysis: whereas quicksort has an expected running time of $\Theta(n lgn)$, the expected running of **RANDOMIZED-SELECT** is $\Theta(n)$, assuming that the elements are distinct.  
```
RANDOMIZED-SELECT(A,p,r,i):
    if (p == r):
        return A[p]
    q = RANDOMIZED-PARTITION(A,p,r)
    k = q - p + 1
    if (i == k) return A[q]
    if (i < k): return RANDOMIZED-SELECT(A,p,q-1,i)
    return RANDOMIZED-SELECT(A,q+1,r,i-k)  
```  
**RANDOMIZED-SELECT(A,p,r,i):** is the function that find the i-th smallest element in the subarray A[p..r]  
**RANDOMIZED-PARTITION(A,p,r):**  is the function that randomly choose p <= q <= r and reform the array such that: all elements in subarray A[p...q-1] are smaller than A[q], all elements in subarray A[q+1...r] is greater than r. It takes total $/Theta(n)$ running time.  
After implementing **RANDOMIZED-SELECT**, we know that A[q] is q-th smallest element in the array. Then, **RANDOMIZED-SELECT** works as follow:  
+ q == i: We find our answer for finding i-th smallest element.  
+ q < i: Our answer belongs to the subarray: A[p...q-1]
+ q > i: Our answer belongs to the subarray: A[q+1..r]   

The worst-case running time for RANDOMIZED-SELECT is $\Theta(n^2)$, even to find the minimum, because we could be extremely unlucky and always partitions around the largest remaining element. However, because is is randomized, no particular input elicits the worst-case behavior.  
To analyze the expected running time of **RANDOMIZED-SELECT**, we let the running time on an input array A[p..r] of n elements is T(n), and we obtain an upper bound on E[T(n)] as follows.  
Because the procedure **RANDOMIZED-PARTITION** is equally likely to return an element as the pivot, therefore the probability that the pivot is k-th smallest number with $1 \leq k \leq n$ is equally as $\frac{1}{n}$.  
$X_{k}$ = I(the pivot is the k-th smallest number).  
$\rightarrow E[X_{k}] = 1/n. (9.1)$  
When we call **RANDOMIZED-SELECT** and choose the A[q] as the pivot element, we fail into three cases:
+ We terminate immediately with the correct answer.
+ We recurse on the subarray A[p...q-1].
+ We recurse on the subarray A[q+1..r].  

Therefore, when we call T(n), and choose k-th smallest element as the pivot, we divide the problem into 2 subarrays after **PARTITION**: A(p, ... p+k-1) and A(p+k+1, q).  
For a given call of **RANDOMIZED-SELECT**, the indicator random vaariable $X_{k}$ has the value 1 for exactly one value of k and it is 0 for all othe k. When X_{k}, the two subarrays on which we might recurse have size k - 1 and n - k. Hence, we have the recurrence:  
$T(n) \leq \sum_{k=1}^{n} X_{k} * max(T(k-1), T(n-k)) + O(n)$    
Assuming that T(n) is monotonically increasing, which means:  
$max(T(k-1), T(n-k)) = T(max(k-1,n-k))$  
$\rightarrow T(n) \leq  \sum_{k=1}^{n} X_{k} * T(max(k-1,n-k)) + O(n)$  
Taking expected values, we have:  

![expectedval](expectedVal.png)  
(C.24):  
"In general, when n random variables $X_{1}, X_{2}, ... X_{n}$ are mutually indepent.  
$E[X_{1}X_{2}...X_{n}] = E[X_{1}]E[X_{2}]...E[X_{n}]$""  
We notice that:  
![maxTn](maxTn.png)  
Thus:
$E[T(n)] \leq \frac{2}{n} \sum_{k = \lceil n/2 \rceil}^{n-1} E[T(k)] + O(n).$  
We show that E[T(n)] = O(n) by substitution. Assume that $E[T(n)] \leq cn$ for some constant c that satisfies the initial conditions of the recurrence.  
We also pick a constant a such that the function described by the O(n) term.  
Using this inductive hypothesis, we have:  
![prove](prove.png)  
In order to complete the proof, we need to show that for sufficiently large n, this last expressio is at most cn or, equivalently that $cn/4 - c/2 - an \geq 0$  

$$\leftrightarrow n \geq \frac{c/2}{c/4 - a} = \frac{2c}{c-4a}$$  
s
Thus, if we assume that T(n) = O(1) for n < 2c/(c-4a).  then E[T(n)] = O(n).  
We conclude that we can find any order statistic, and in particular the median, in expected linear time, assumsing that the elements are distinct.  
## 9.3. Selection in worst-case linear time.  
We now examine a selection algorithm whose running time is O(n) in the worst case.  
The SELECT algorithm determines the *i*th smallest of an input array of n > 1 distinct elements by executing the following steps.  
+ Divide n elements into $\lfloor n/5 \rfloor$ group of 5 elements and at most one group made up of the remaining n mod 5 elements.  
+ Find the median of each of the $\lfloor n/5 \rfloor$ groups by first insertion-sorting the elements of each group and then picking the median from the sorted list of groups elements.
+ Use SELECT recursively to find the median x of the $\lfloor n/5 \rfloor$ fond n step 2. (If there are an even number of medians, then by our convention, x is the lower median).  
+ Partition the input array around the median-of-medians using modified version of PARTITION. Let k be one more than the number of elements on the low side of the partition, so than x is the kth smallest element.  
+ If (i == k) then return x. Otherwise, use SELECT recursively to find i-th smallest element on the low-side if (i < k), ir the (i-k) th smallest number on the high side if i > k.  
I will not dig deep in [9.3] because I find it is not actually relevant to what we focus on this chapter, so if you want, please refer to page 242-243 in the download version.  

**Exercises**  
**9.3-1:** In the algorithm SELECT, the input elements are divided into groups of 5. Will the algorithm work in linear time if they are divided into groups of 7. Argue that SELECT does not run in linear time if groups of 3 are used.  
**Solution:**  
Because we takes the median-of-medians as pivot then there are at least [n/14] medians are smaller than the pivot. And in each of groups according to [n/14] medians, there are at least 4 elements are smaller than our pivot.  
So the pivot is greater than at least [4n/14] elements, and at most smaller than [10n/14] elements.  
Therefore, we have the formula:
$ T(n) \leq T(n/7) + max (T(10n/14), T(4n/14)) + O(n). $
We can prove this by using substitution method.  
Suppose $T(n) \leq cn$ for n < k, then for $m \geq k$ we have:  
$ T(m) \leq T(m/7) + T(10m/14) + O(m) \leq cm(1/7 + 10/14) + O(m).  

Suppose now that we use groups of 3 instead. So we have another recursion:  
T(n) = $T(\lceil n/3 \rceil) + T(4n/6) + O(n) \geq T(n/3) + T(2n/3) + O(n).$  
So we will show it is $\geq cnlg(n)$.  
**9.3-3:** Show how quicksort can be made to run in O(nlogn) time in the worst case, assuming that all elements are distinct.  
**Solution:**  
First we find the median by partioning, then quicksort.  
T(n) = 2T(n/2) + O(n).  
**9.3-4:** Suppose that an algorithm uses only comparisons to find the i-th smallest element in a set of n elements. Show that it can also find the i-1 smaller elementss and the n-i larger elements without performing any additional comparisions.  
**Solution:**  
Draw a directed graph includes n nodes represent for n elements, which has a edge from node i to node j means than A[i] >= A[j].  
When when find the i-th smallest number, which means than our graph is an Directed acyclic graph.  
Call the node represents for the i-th smallest element is x.     
In order to determine the i-th smallest numbers, all other nodes j must have a way to go from j to x or from x to j, otherwise, we cannot determine A[j] >= A[x] or not.  
Therefore, any node that have a road to the node x is greater than i-th smallest element, otherwise, it is smaller.  
**9.3-7:** Describe an O(n)-time algorithm that given a set S of n distint numbers and a positive integer k $\leq$ n, determines the k numbers in S that are closest to then median of S (according to position).  
**Solution:**  
- Find the [n/2] - [k/2]-th largest number. Partitioning around by choosing it as pivot.  
- Find the k-th largest number in the bigger subset, after partitioning, our answer is the subset that have k-1 numbers.  
**9.3-8:** Let X[1...n] and Y[1...n] be two arrays, each containing n numbers already in sorted order. Give an O(lg n)-time algorithm to find the median of all 2n elements.  
**Solution:**  
Without loss of generality, assume n is a power of 2.
```
Median(X,Y,n):
    if n == 1:
        return min(X[1], Y[1])
    if X[n/2] < Y[n/2]: return Median(X[n/2]+1...n), Y[1...n/2], n/2)
    return Median(X[1..n/2], Y[n/2+1..n], n/2)
```  




















