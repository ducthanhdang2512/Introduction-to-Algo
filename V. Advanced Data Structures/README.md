# 21. Data Structures for Disjoint Sets.  
## 21.1. Disjoint-set operations.  
A **disjoint-set data structure** maintains a collection S = ${S_1,S_2...S_k}$ of disjoint dynamic sets. We identify each set by a **reprensentative**, which is some member of the set.  
These following operations are required for DSU:  
INIT procedure.  
```
void init(){for(int i=1;i<=n;++i) pset[i] = i;}
```  
FIND-SET procedure: find the representatitve for a member.    
```  
int finds(int x){return (pset[x] == x ? x : pset[x] = finds(pset[x]));}
```  
UNION(x,y) procedure: join two sets of member together.  
```
int unions(int x,int y){ pset[finds(x)] = finds(y); }
```  
One application for DSU is connecting a graph, more interestingly, we can use them to create MST.  
## 21.2. Linked-list representation of disjoint sets.  
Because, we are not intent to use linked list to represent DSU, therefore, If you wish, you might refer to the source at pages 564.  
## 21.3. Disjoint-set forests.  
In disjoint-set forests, each members points only to its parent. The root of each tree contains the representative and is its own parent.  
**Heuristics to improve the running time**  
We will two heuristics:    
- **union by rank**: The approach is make the root of the tree with fewer nodes point to the root of the tree.  
- **path compression**: During FIND-SET operations to make each node on the find path point directly to the root. Path compression does not change any ranks.  
By applying these two heuristic, the running time decrease greatly to O(m *$\alpha(n)$).  
## 21.4. Analysis of union by rank with path compression.  
Currently, learning to analyze these things are not my target for learning, so If you want to study, please refer to page 573.   