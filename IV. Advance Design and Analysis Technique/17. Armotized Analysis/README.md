# 17. Amortized Analysis.  
In  an **amortized analysis**, we average the time required to perform a sequence of data-structure operations over all operations performed. With **amortized analysis**, we can show the average cost of operations are small.  
The first three sections of this chapter cover the three most common techniques used in amortized analysis.  
+ Section 17.1: Aggregate analysis, in which we determine an upper bound T(n) on the total cost of a sequence of n operations. --> The average cost is T(n)/n.  
+ Section 17.2 covers the accounting method, in which we determine an amortized cost of each operation. The accounting method overcharges some operations early in the sequence, storing the overcharge as "prepaid credit". Later in the sequence, the credit pays for operations that are charged less than they actually cost.  
+ Section 17.3 discusses the potential method, which is like the accounting method in that we determine the amortied cocst of each operation and my overcharge operations early on. The potential method  maintains the credit as the "potential energy" of the data structure instead of associating the credit with individual objects within data structure.  
While reading this chapter, bear in mind that the charges assigned during amortized analysis are for analysis purposes only.  
## 17.1. Aggregate analysis.  
**Stack operations**  
In our first example of aggregate analysis, we analyze stacks that have been augmented with new operations. Section 10.1 presented the two fundametal stack operations.  
+ PUSH(S,x)  
+ POP(S)
+ Now we add one more operations:`MULTIPOP(S,k)` which removes the k top objects of stack S.  

The cost of an `MULTIPOP(S,k)` is O(k), however, when you use **Aggregate analysis**, you can see the cost of n operations still is O(n) - which is limited to the size of the stack.  
--> average cost of `MULTIPOP(S,k)`: O(1).  
**Incrementing a binary counter**  
> Consideer the problem of implementing a k-bit binary counter that counts upward from 0. We use an array A[0...k-1] of bits, where A.length = k, as the counter.  
```
INCREMENT(A):
i = 0
while i < A.length and A[i] == 1.
    A[i] = 0 // flip
    i = i + 1
if i < A[i].length
    A[i] = 1. // flip
```  

![incrementing](incrementing.png)  

The cost of each `INCREMENT(A)` is O(k), but we observe that in each operations, not entirely k bits flips.  
As in Figure 17.2 above, you can observe that the bits A[0] flips every 1 `INCREMENT` operations, A[1] flips every 2 `INCREMENT` operations, A[2] flips every 4 `INCREMENT` operation.  
Therefore we have the total number of flips in the entire sequence is thus:  

$$\sum_{i = 0}^{k=1}[\frac{n}{2^i}] < n\sum_{i=0}^{\infty} \frac{1}{2^i} = 2n$$  
Therefore, the average cost of each operation is: O(n)/n = O(1).  
## 17.2. The accounting method.  
In **accounting method** of amortized analysis, we assign differing charges to different operations, with some operations charged more or less than they actually cost. We call the amount we charge on operation its **amortized cost**.  
We must choose the amortized costs of operations carefully. If we want to show that in the worst case the average cost per operation is small by analyzing the amortized costs, we must ensure that the total amortized cost of a sequence of operations provides an upper bound on the total actual cost of the sequence.  
**Stack operations**  
To illustrate the accounting method of amortized analysis, let us return to the stack example. Recall that the actual costs of the operations were:  
PUSH: 1  
POP:  1  
MULTIPOP: min(k.s)  
Let us assign the following amortized costs:  
PUSH:      2  
POP        0  
MULTIPOP   0  
By re-assigning the cost of each operations, we now can prove the average cost of each operations is O(1).  
**Incrementing a binaray counter**  
To be simple, we call a cost of an O(1) operation is 1 dollar.    
Now we shall recall the actual costs of the operations flip. We divide them into two type.  
Flip 0 -> 1: 1
Flip 1 -> 0: 1
Now, let assign the following amortized costs:
Flip 0 -> 1: 2
Flip 1 -> 0: 0
The meaning behinds the amortized costs is when we flip a bit into 1, we overcharge it with 2 dollars (1 for flip into 1, 1 for when flipping it back into 0).  
--> While the `INCREMENT` procedure flip a bit 0 into 1 only one time each operations, the total costs of n opertations is O(n), thus the average cost is O(1).  
## 17.3. The potential method  
Instead of representing prepaid work as credit stored with specific objects in the data structure, the **potential method** of amortized analysis represents the prepaid work as "potential energy", or just "potential", which can release to pay for future operations.  
The potential method works as follows. We will perform n operations, starting with an initial data structure $D_0$. For each i = 1,2...n, we let $c_i$ be the actual cost of the ith operation and $D_i$ be the data structure that results after applying the i-th operation to data strcuture $D_{i-1}$. A **potential function** $\Phi$ maps each data structure $D_i$ to a real number $\Phi(D_i)$, which is the potential associated with data structure $D_i$. The **amortized cost** $C_i$ of the ith operation with respect to potential function $\Phi$ is defined by:

$$ C_i = ci + \Phi(D_i) - \Phi(D_{i-1}) (17.2)$$  

$$\rightarrow \sum_{i = 1}^{n} C_i = \sum_{i=1}^{n} ci + \Phi(D_n) - \Phi(D_0) (17.3)$$    

If we can define a potential function $\Phi$ so that $\Phi(D_n) \geq \Phi(D_0)$, then the total amortized cost $\sum_{i=1}^{n} C_i$ gives an upper bound on the total actual cost $\sum_{i=1}^{n} ci$.  
**Stack operations**  
To illustrate the potential method, we return once again to the example of the stack operations PUSH, POP and MULTIPOP. We define the potential function $\Phi$ on a stack to be the number of objects in the stack. For empty stack $D_o$ with which we start, we have $\Phi(D_0) = 0$. Since the number of objects in the stack is never negative, the stack $D_i$ that results after the ith operation has nonnegative potential, and thus:  
$\Phi(D_i) \geq 0 = \Phi(D_0)$  
The total amortized cost of n operations with respect to $\Phi$ therefore represents an upper bound on the actual cost.  
Let us now compute the amortized costs of the various stack operations. If the ith operations on a stack containing s objects is a PUSH operation, then the potential difference is:  

$$\Phi(D_i) - \Phi(D_{i_1}) = (s+1) - s = 0 $$  

By equation (17.2), the amortized cost of this PUSH operation is:  
$C_i = c_i + \Phi(D_i) + \Phi(D_{i-1}) = 1 + 1 = 2$  

Suppose that the ith operation on the stack is MULTIPOP(S,k), which causes k' = min(k,s) objects to be popped off the stack. the actual cost of operation is k', and the potential different is:  
$\Phi(D_i) - \Phi(D_{i-1}) = -k'$  
Thus, the amortied cost of the MULTIPOP operation is:  
$C_i = c_i + \Phi(D_i) + \Phi(D_{i-1}) = k' - k' = 0$  
Similarly, the amortized cost of an orginary POP operation is 0.  
The amortized cost of each of the three operations is O(1), and thus the total amortized cost of a sequence of n operations is O(n). Since we have already argued that $\Phi(D_i) \geq \Phi(D_0)$, the total amortized cost of n operations is an upper bound on the total actual cost. The worst-case of n operations is therefore O(n).  
**Incrementing a binary counter**  
As another example of the potential method, we again look at incrementing a binary counter. This time, we define the potential of the counter after i-th INCREDIMENT operation to be $b_i$, the number of 1s in the counter after ith operation.  
Let us compute the amortized cost of an INCREMENT operation. Suppose that the ith INCREMENT operation resets $t_i$ bits. The actual cost of the operations is therefore at most $t_i + 1$, since in addition to resetting $t_i$ bits, it set at most one bit to 1. If $b_i$ = 0 then the ith operaation resets all k bits, and so $b_{i-1} = t_i = k$. If $b_i > 0$, then $b_i = b_{i-1} - t_i + 1$. In either case, $b_i \leq b_{i-1} - t_i + 1, and the potential different is:  

$\Phi(D_i) - \Phi(D_{i-1}) \leq (b_{i-1} - t_i + 1) - b_{i-1} = 1 - t_i$  

The amortized cost is therefore:  

$C_i = c_i + \Phi(D_i) - \Phi(D_{i-1}) \leq (t_i + 1) + (1 - t_i) = 2.$  

If the counter starts at zero, then $\Phi(D_0) = 0$. Since $\Phi(D_i) \geq 0$ for all i, the total amortized cost of a sequence of n INCREMENT operations is an upper bound on the total actual cost, and so the worst-case cost of n INCREMENT operations is O(n).  
The potential method gives uss an easy way to analyze the counter even when it does not start at zero. The counter starts with $b_0$ 1s, and after n INCREMENT operations it has $b_n$ 1s, where $0 \leq b_0,b_n \leq k$. (Recall that k is the number of bits in the counter.) We can rewrite equation (17.3) as:  

$$\sum_{i = 1}^{n} C_i = \sum_{i=1}^{n} ci + \Phi(D_n) - \Phi(D_0) (17.3)$$   

We have $C_i \leq 2$ for all $ 1 \leq i \leq n$. Since $\Phi(D_0) = b_0$ and $\Phi(D_n) = b_n$, the total actual cost of n INCREMENT operations is:

$$\sum_{i = 1}^{n} C_i = \sum_{i=1}^{n} 2 - b_n + b_0 (17.3) = 2n - b_n + b_0$$  

Note that in particular that since $b_0 \leq k$ as long as k = O(n), the total actual cost is O(n).  

Personally, If you want to understand more about amortized analysis, it is recommended to read chapter 17.4. However, due to personal convenience, I will stop my editting in there, because I believe that it is enough for you to understand the analysis in algorithm.  









