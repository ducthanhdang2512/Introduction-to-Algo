# 16. Greedy Algorithms.
Algorithms for optimization problems typically go through a sequence of steps, with a set of choices at each step.  
A **greedy algorithm** always makes the choice that looks best *at the moment*. Before, reading this chapter, you should read about dynamic programming in Chapter 15, particularly Section 15.3.  
Greedy algorithms do not always yield optimal solutions, but for many problems, they do.  
- **Section 16.1** will examine a simple but nontrivial problem, the activity-selection problem, for which is a greedy algorithms efficiently computes an optimal solution.
- **Section 16.2** reviews the basic elements of the greedy approach, giving a direct approach for proving greedy algorithm correct.
- **Section 16.3** presents an important application of greedy techniques: Designing data-copression (Huffman) codes.
- **Section 16.4** will investigate some of the theory underlying combinatorial structures called "matroids".
- Finally, **Section 16.5** applies matroids to solve a problem of scheduling unit-time tasks with deadlines and penalties.  

The greedy method is quite powerful and works well for a wide range of problems. Later chapteres will present many algorithms that we can view as application of the greedy method, including minimum-spanning-tree algorithms (Chapter 23), Dijkstra's algorithm for shortest paths from a single source (Chapter 24) and Chvátal's greedy set-covering heuristic (Chapter 35).  
You might find it useful when read chapter 23(Minimum-spanning-tree) together with this chapter.  
## 16.1 An activity-selection problem.  
**Problem statements**
> Suppose we have a set S = ${a_1, a_2, ... a_n}$ of n proposed **activities**. Each activities $a_i$ has a **start time** $s_i$ and a **finish time** $f_i$, where $0 \leq s_i < f_i$ . If selected, activity $a_i$ takes place during [$s_i, f_i$).  
Activities $a_i$ and $a_j$ are **compatible** if [$s_i, f_i$) and [$s_j, f_j$) do not overlap. That is, $a_i$ and $a_j$ are compatible if $s_i \geq f_j$ or  $s_j \geq f_i$  
We wish to select a maximum-size subset of mutally compatible activites. We assume that the activities are sorted in monotically increasing order of finish time.  

**The optimal substructure of the activity-selection problem.**  
We can easily verify that the activity-selection problem exhibits optimal substructure.  
Let us denote by $S_{ij}$ the set of activiteis that start after activities $a_i$ finishes and finished before $a_j$ start.  
Call $A_{ij}$ is the maximum set of mutually compatible activities in $S_ij$  
Suppose an activities k within set S_ij. Then we have:  

$$\mid A_{ik} \mid  + \mid A_{kj} \mid + 1  = \mid A_{ij} \mid $$  

Therefore we have such an solution:
Consider c[i,j] is the size of the optimal solution of $S_{ij}$  
If $S_{ij} != \emptyset$:  
    c[i,j] = max (c[i,k] + c[k,j]) ($a_k \in S_{ij}$)
else: c[i][j] = 0  
We could the develop a recursive algorithm and memoize it, or we could work bottom-up and fill the table entries as we go along.  
However, we would be overlooking another important characteristic of the activity-selection problem that we can use to great advantage.  
**Making the greedy choice**  
What if we could choose an activity to add to our optimal solution without having to first solve all the subproblems. In fact, for the activity-selection problem, we need to consider only one choice: the greedy choice.  
What do we mean by the greedy choice for the activity-selection problem? Intuition suggests that we should choose an activities that leaves the resource available for as many other activities as possible.  
Therefore, we come up with an greedy solution:
- Step 1: we need to choose the action that finish first, because it will "leave" more resource for other activites to fill in. Because our set is sorted by the finished time, our choice will be $a_1$.    
- Step 2: after choose $a_1$, we also need to find the action $a_j$ which start after $a_1$ finished and have smallest finish time.  
- Step 3, repeat step 2 till when you can't other activites to add to your solution.   

To prove that our intuition is correct, I will leave to readers.  
As we can see, to implement a correct solution, it is not neccessary to always run through every possible situation. In other hand, an algorithm to solve the activity-selection problem does not need to work bottom-up, like a table-based DP solution. Instead, it can work top-down with the greedy solution.  
**An iterative greedy algorithm**  
Below is my solution for the activation selection problem.  
Set $a_{k}$ is the current last member in our optimal solution. Because the finish time is sorted in increasing order, we will need to find smallest `i` such that $s_i \geq f_k$.  
Then, set `k = i`.  

```
GREEDY-ACTIVITY-SELECTOR(s,f):
    n = s.length  
    A = {a_1}
    k = 1
    for m = 2 to n:
        if (s[m] >= f[k]):
            A.add(a_m)
            k = m
    return A
```
This greedy algorithm work in $\Theta(N)$  
### Excercise:  
**16.1-2:** Suppose that instead of always selecting the first activity to finish, we instead select the last activity to start that is compartible with all previously selected activities. Describe how this approach is a greedy algorithm, and prove that it yields an optimal solution.  
**Solution:**
This appoach is an reverse version of the greedy solution in Section 1.  
**16.1-4:** Suppose that we have a set of activities to schedule among a large number of lecture halls, where any activity can take place tin any lecture hall. We wish to schedule all the activities using as few lecture hall as possible. Given an efficient greedy algorithm.  
**Solution:**  
This problem is also known as the **interval-graph coloring problem**. We can create an interval graph whose vertices are the given activities and whose edges connect incompatible activities. The smallest number of colors required to color every vertex so that no two adjacent vertices have the same color. The answer is the smallest number of color required.  
Greedy approach:  
Maintain a set of free (but already used) lecture halls F, and currently busy lecture halls B. Sort the classes by start time. For each new start time which you encounter, remove a lecture hall from F, schedule the class in that room and add the lecture hall to B. If F is empty, add a new, unused lecture hall to F. When a class finishes, remove its lecture hall from B and add it to F.  
Why this is optimal: Suppose we have just started using the m-th lecture hall for the first time. This only happens when ever classroom ever used before is in B. But this means that there are m classes occuring simultaneously, so it is necessary.  
## 16.2. Elements of the greedy strategy
How can we tell whether a greedy algorithm will solve a particular optimization problem? No way works all the time, but the greedy-choice property and optimal substructure are the two key ingredients.  
### **Greedy-choice property**.  
the first key ingredient is the **greedy-choice property**: we can assemble a globally optimal solution by making locally optimal (greedy) choice. In other words, when we are considering which choice to make, we make the choice that looks best in the current problem, without considering results from subproblems.  
Of course, we must prove that a greedy choice at each step yields a globally optimal solution.  
### **Optimal substructure**  
A problem exhibits **optimal substructure** if an optimal solution to the problem contains within it optimal solution to subproblems. This property is a key ingredient of assessing the applicability of dynamic programming as well as greedy algorithms.  
### **Greedy versus dynamic programing**  
Because both the greedy and dynamic-programming stategies exploit optimal sub-structure, you might be tempted to generate a D solution to a problem when a greedy solution suffices or, conversely, you might mistakenly think that a greedy solution might works when in fact a dynamic programming solution is required.  
You need to careful to distinguish between these twos.  
## 16.3. Huffman codes.  
Huffman codes compress daata vvery effectively: saving of 20% to 90% are typical, depending on the characteristics of the data being compressed.  
We consider data to be a sequence of characters. Huffman's greedy algorithm uses a table giving how often each character occurs (i.e. its frequency) to build up an optimal way of representing each character as a binary string.  
Suppose we have a 100.000 character data file that we wish to store compactly. We observe that the characters in the file occur with the frequency given by Figure 16.3. That is, only 6 different characters appear, and the character a occurs 45.000 times.  

![Figure163](figure163.png)  

We have many options for how to represent such a file of information. Here, we consider the problem of designing a **binary character code** (or **code** for short) in which each character is represented by a unique binary string, which we call codeword. If we use a **fixed-length code**, we need 3bits to represent 6 characters: a = 000, b = 001, ... f = 101. This method requires 300000 bits to code entire file, can we do better?  
A **variable-length code** can do considerably better than a fixed-length code, by giving frequent characters short codewords and infrequent characters long codewords. You can look in figure 16.3 above.  
In this case, by using **variable-length code**, we can make our answer better:  
$(45.1 + 13.3 + 12.3 + 16.3 + 9.4 + 5.4) . 1000 = 224000 bits.
It is much smaller than the original answer.  
Using Huffman code, we can answer these question.  
### **Prefix codes**  
We consider here only codes in which no codeword is also a prefix of some other codewords.  Such codes are called **prefix codes**. Although we won't prove it here, a prefix code can always achieve the optimal data compression among any characters code, and so we suffer no loss of generality by restricting our attention to prefix code.  
Prefix codes are desirable because they simplify decoding. Since no codeword is a prefix of any other, the codeword that begins an encoded file is unambigous. We can simply indentify the initial codeword, translate it back to original character, and repeat the decoding process on remainder of the encoded file. In our example, the sstring 001011101 parses uniquely as 0.0.101.1101 which decodess to aabe.  

![decoding](decoding.png)  

An optimal code for a file is always represented by a *full* binary tree, in which every nonleaf node has two children (see Exercise 16.3-2). The fixed-length code in our example is not optimal since its tree, show in Figure 16.4a above, is not a full binary tree.  
Since we can now restrict our attention to full binary trees, we can say that if C is the alphabet from which the characters are drawn and all character frequencies are positive, then the tree for an optimal prefix code has exactly $\mid C \mid$ leaves, one for each letter of the alphabet, and exactly $\mid C \mid$ - 1 internal nodes. (See Exercise B.5-3).  
Given a tree T corresponding to a prefix code, we can easily compute the number of bits required to encode a file. For each character c in the alphabet C, let the attribute c.freq denote the frequency of c in the file and let $d_r(c)$ denote the depth of c's leaf in the tree. Note that $d_r(c)$ is also the length of the codeword for character c. The number of bits required to encode a file is thus.  

$$B(T) = \sum_{c \in C} c.freq * dr(c) (16.4)$$  

Which we define as the **cost** of the tree T.  
### **Constructing a Huffman code**  
Huffman inveted a greedy algorithm that constructs an optimal prefix code call a **Huffman code**  
The core idea of **Huffman code** is:
- Choose two leafs with lowest **frequency**.  
- Merge them into a new leafs, and its frequency is equal to the sum of two choosen leafs.
- Repeat these process $\mid C \mid - 1$ times, the final answer is the cost of Huffman codes.  
- We use min-heap to accomplish the Huffman code.  
```
HUFFMAN(C):
    n  = C.size
    Q = C
    for i = 1 to n - 1
    allocate a new node z
    z.left = x = EXTRACT-MIN(Q)
    z.right = y = EXTRACT-MIN(Q)
    z.freq = x.freq + y.freq
    INSERT(Q.z)
return EXTRACT-MIN(Q) // return the root of the tree.  
```
Figure 16.5 will show the progress of Huffman's code.  

![huffmancode](huffmancode.png)  

### **Correctness of Huffman's algorithm**  
**Note:** This is my personal comment about Huffman code, which is not the original prove by the author. If you want to have the perfect prove, let go to the original source.  
As we should see, we answer of Huffman codes is optimal only when: the frequency of the root is optimal, which means, the freq of these $\mid C \mid - 1$ nodes, which is not a leaf, should be optimal.  
Also, we knew that each of $\mid C \mid - 1$ non-leaf nodes, is combined by two childs. Therefore, intuitively, we need to choose two **current** leaf which have smallest frequency, and merge them together.  
### **Exercises**  
**16.3-2** Prove that a binary tree that is not full cannot correspond to an optimal prefix code.  
Suppose that we have a node c that have only one child.  
Call the depth of c is d(c).    
We can see that, because, the goal of building the tree is to represent the binary code of words.  
the "goal" of each node `c`, which is not a leaf, is to "differ" at least two "leaf". Because, c has only one child, i failed to "differ" two different leaves.  
For example: 0010, and 0011. You can see that the 2th '0' is "unnecessary", because we can just ignore it and still is able to differ two characters (0010 --> 010, 0011 --> 011).   
## 16.4. Matroids and greedy methods.  
In this section, we sketch a beautiful theory about greedy algorithms. This theory describes many situations in which the greedy method yields optimal solutions. It involves combinatorial structures known as "matroids." Although this theory does cover the activity-selection problem of Section 16.1 or the Huffman-coding problem of Section 16.3), It does cover many cases of practical interest. Furthermore, this theory has been extended to cover many applications,; see the notes at the end of this chapter for references.  
### **Matroids**
A ***matroid*** is an ordered pair M = (S, I) satisfying the following conditions.  
- S is a finite set.  
- I is nonempty family of subsets of S, called the **independent** subsets of S, such that if B $\in$ A and A $subseteq$ B, then A $\in$ B. We say that **I** is a **hereditary** if it satisfies this property. Note that the empty set $\empty$ is neccessarily a member of *I*.  
- If $A \in B$, $B \in I$, and $\mid A \mid < \mid B \mid$, then there exists some element $x \in B - A$ such that $A \cup {x} \in **I**$. We say that M satisfies the **exchange property**.  
The word "matrod" is due to Hassler Whitney. He was studying ***matrix matroids***, in which the elements of S are the rows of a given matrix and a set of rows is independent if they are linearly independent in usual sense. As Exercose 16.4-2 asks you to show, this strucutre defines a matroid.  
As another example of matroids, consider the *graph matroid* $M_G = *(S_G, I_G)*$ defined in terms of a given undirected graph G = (V, E) as follows:  
- The set of $S_G$ is defined to be E, the set of edges of G.  
- If A is a subset of E, then A $\in I_G$ if and only if A is acyclic. That is, a set of edges are A is independent if and only if the subgraph $G_A = (V,A)$ forms a forest.  
The graphic matroid $M_G$ is closely related to the minimum-spanning-tree problem, which Chapter 23 covers in details.  

**Theorem 16.5**  
> If G = (V, E) is an undirected graph, then *$M_G = (S_G,I_G)$ is a matroid.  

**Proof**  
Clearly, $S_G = E$ is finite set.
Furthermore, $I_G$ is hereditary, since a subset of a forest is a forest.  
Thus, i remains to show that $M_G$ statisfies the exchange property. Suppose that $G_A = (V, A)$ and $G_B = (V, B)$ are forests and |B| > |A|. That is, A and B are acyclic sets of edges, and B contains more edges than A does.  
We claim that a forest F =($V_F, E_F$) contains exactly $|V_F| - |E_F|$ trees.  
Since forest $G_B$ has fewer trees than forest $G_A$ does, forest $G_B$ must contains some tree *T* whoese vertices are in two different trees in forrest $G_A$. Moreover, since *T* is connected, i must contain an edge (u, v) such that u and v are in different trees in forest $G_A$. Since the edge (u,v) connects vertices in two different trees in $G_A$, we can add edge (u,v) to forrest $G_A$ without creating a cycle. Therefore, $M_G$ satisfies the exchange property, completing the proof that $M_G$ is a matroid.  
Given a matroid M = (S, I), we call an element $x \in A$ an extension of $A \in I$ if we can and x to A while preserving independence, which means $A \cup {x} \in **I**$.  
If A is an independent subset in a matroid M, we say that A is **maximal** if it has no extensions. That is, A is maximal if it is not contained any larger independent subset of M. They following property is often useful.  
**Theorem 16.6**  
> All maximal independent subsets in a matroid have the same size/  

**Proof**  
Using **exchange property**, we can see that all maximal independent subsets in a matroid have the same size.  
As an illustraton of this theorem, the maximal independent subsets of **M_G** is called a **spanning tree** of G.  
**Greedy algorithms on a weighted matroid**  
We say that a matroid M = (S, I) is **weighted** if it is associated with a weight function that assigns a strictly postive weight w(x) to each element $x \in S$. The weight function w extends to subsets of S by summation:  

$$w(A) = \sum_{x \in A} w(x)$$

for any $A \subseteq S$, we can look **minimum-spanning-tree** as an problem finding the optimal subset of a matroid.  
Chapter 23 gives algorithms for the minimum-spanning-tree problem, but here we give a greedy algorithms that works for any weighted matroid. The algorithm takes as input a weighted matroid M = (S, I) with an associated postive weight function w, and it returns an optimal subset A.  

![matroid1](matroids1.png)  

This greedy method work similarly with **minimum-spanning-tree** algorithm.  
**Lemma 16.7 (Matroids exhibit the greedy-choice property)**  
> Suppose that M = (S, I) is a weighted matroid with weight function w and that S is sorted into monotically decreasing order by weight. Let x be the first element of S such that {x} is independent, if any such x exists. If x exists, then there exists an optimal subset A of S that contains x.  

**Proof**  
If no such x existsm then the only independent subset is the empty set and the lemma is vacuaously true.  
Otherwise, let B be any nonempty optimal subset of . Assume that x $\notin$ B; otherwise, letting A = B givess an optimal subset of S that contains x.  
No elements of B has weight greater than {x}. To see why, observe that $y \in B$, implies that {y} is independent, since $B \in I$ and I is hereditary. Because {x} is the first set have independent property, then we can see that $w(y) \leq w(x)$  
Construct the set A by using **exchange property**, we will get the set |A| = |B|, while preserving the independence of A. At that point, A and B are the same except the A has x and B has some other element y.  

$$w(A) - w(b) - w(y) + w(x) \geq w(B)$$  

Because, set B is optimala, set A, which contains x, must also be optimal.  

We next show that if an element is not an option initially, then it cannot be an option later.   
**Corrollary 16.9**  
> Let M = (S, I) be any matroid. If x is an element of S such that x is not an extension of $\empty$, then x is not an extension of any independent subset A of S.  

Corrollary 16.9 says that any element cannot be used immediately can never be used. Therefore, GREEDY cannot make an error by passing overe any intial elements in S that are not an extension of $\empty$, since they can never be used.  
**Lemma 16.10 (Matroids exhibit the optimal-substructure property**  
> Let x be the first element of S chosen by GREEDY for the weighted matroid M = (S,I).  The remaining problem of finding a maximum-weight independent subset contain x reduces to finding a maximum-weight independet subset of the weighted matroid M', where:  
S' = ${y \in S, {x,y} \in I}$  
I' = ${B \subseteq S = {x}: B \cup {{x}} \in I}$   

**Theorem 16.11 (Correctness of the greedy algorithm on matroids)**  
If M = (S, I) is a weighted matroid with weight function w, then GREEDY(M,w) returns an optimal subset.  
**Proof**  
Using the above lemmas and corrolary 16.9, we can prove the theorem and thus prove the GREEDY choice is correct.  
## 16.5. A task-scheduling problem as a matroid.  
**Problem statement**  
> A ***unit-time task*** is a job, such as a program to be run on a computer, that requires exactly one unit of time to complete.  
The problem of ***scheduling unit-time tasks with deadlines and penalties for a single processor** has the following inputs:  
    + A set S = {$a_1, a_2, ..., a_n$} of n unit-time tasks;  
    + A set of n integer **deadlines** $d_1, d_2, ... d_n$ such that each $ 1 \leq d_i \leq n$ and task $a_i$ is supposed to finish by time $d_i$; and  
    + A a set of nonnegative weights or ***penalty*** $w_1, w_2, ...w_n$; such that we incur a penalty of $w_i$ if task $a_i$ is not finished by time $d_i$  
We wish to find a schedule for S that minimizes the total penalty incurred for missed deadlines.  

Consider a given schedule. We say that a task is **late** in this schedule if it finishes after its deadline. Otherwise, the task is **early** in the schedule. We can always transform an arbitrary schedule into **early-first form**, in which the early tasks precede the late task. To see why, note that if some early task $a_i$ follows some late task $a_j$, then we can switch the positions of $a_i$ and $a_j$ of $a_i$ and $a_j$, $a_i$ still be early and $a_j$ still be late.  
Furthermore, we claim that we can always transform an arbitrary schedule into **canonical form**, in which the early tasks precede the late tasks and we schedule the early tasks in order of monotically increasing deadlines.  
The search for an optimal schedule thus reduces to finding a set A of tasks that we assign to be early in the optimala schedule. Having determined A, we can create the actual schedule by listing the elements of A in order of monotically increasing deadlines, then listing the late tasks (i.e. S - A) in any order.  
We say that a set A of tasks is **independent** if there exists a schedule for these tasks such that no tasks are late. Clearly, the set of early tasks for a schedule forms an independent set of tasks. Let I denote the set of all independent sets of tasks.  
Consider the problem of determining whether a given set A of tasks is independent. For t = 0,1,2...n let $N_t(A)$ denote the number of tasks in A whose deadline is t or earlier. Note that $N_0(A) = 0$ for any set A.  
**Lemma 16.12**  
> For any set of tasks A, the following statements are equivalent.  
    1. The set A is independent.  
    2. For t = 0,1,2...n, we have $N_t(A) \leq t$.  
    3. If the tasks in A are scheduled in order of monotically increasing deadline, then no task is late.  

**Theorem 16.13**  
> If S is a set of unit-time tasks with deadlines, and I is the set of all independent sets of tasks, then the corresponding system (S, I) is a matroid.  
**Proof**  
- Every subset of an independent set of tasks is certainly independent.  
- To prove the exchange property, suppose that B and A are independent sets of tasks and that |B| > |A|. Let k be the largest t such that $N_t(B) \leq N_t(A)$. Since $N_n(B) = |B|$ and $N_n(A) = |A|$, but |B| > |A|, we must have k < n that $N_j(B) > N_j(A)$ for all j in the range $k+1 \leq j \leq n$. Therefore, B contains more tasks with deadline k+1 than A does. Let a_i be the task in B - A with deadline k+1. Let $A' = A \cup a_i$.  
We now show that A' must be independent by using property 2 of Lemma 16.12. For $0 \leq t \leq k$, we have $N_t(A') = N_T(a) <= t$, since A is independent. For $k < t \leq n$, we have $N_t(A') \leq N_T(B) \leq t$ since B is independent. Therefore, A' is independent, completing our proof.  
By Theorem 16.11, we can use a greedy algorithm to find maximum-weight independent set of tasks A. We can the create an optimal schedule having tasks in A as its early tasks. This method is an efficient algorithm for scheduling unit-time tasks with deadlines and penalties for a single processor. The running time is O(n^2).  
We can also use Disjoint-set union to reduce the running time to O(N).  




























