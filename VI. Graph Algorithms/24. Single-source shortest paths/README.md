# 24. Single-Source Shortest Paths.  
In this chapter and in Chapter 25, we show how to solve shortest path problems efficiently.  
We define d(u,v) is the shortest path weight from u to v (If you can not come to v from u, then d(u,v) = $\infty$).  
The BFS algorithm from section 22.2 is a shortest-paths algorithm that works on underweighted graphs, that is, graphs in which each edge has unit weight.  
The algorithm for the single-source problem can solve many other problems, including the following variants.  
- **Single destination shortest-paths problems**  
- **Single-par shortest-path problem**  
- **All-pars shortest paths problem**: this will be mentioned later in chapter 25.  
**Optimal substructure of a shortest path.**  
**Lemma 24.1. Subpaths of shortest paths are shortest paths**  
- Negative weight edges.
- Cycles: The problem arise when exist a negative cycle in the graph. In order to have more details, please refer to page 647.  
## 24.1. The Bellman-Ford algorithm  
The **Bellman-Ford algorithm** solves the single-source shortest paths problems in general case.  
In short, Bellman-Ford algorithm can be described as following. Running through all edges, and then update the answer by the dynamic programming function: `d[u][v] = min(d[u][k] + d[k][v])`.  
My short version of **Bellman-Ford algorithm** code:  
```
for(int k=1;k<=n;++k){ // If there is no cycle, there are at most n-1 length paths.  
    for(int u=1;u<=n;++u){
        for(int v=1;v<=n;++v)
            d[u][v] = min(d[u][v], d[u][k] + d[k][v]);
    }
}
```  

 



